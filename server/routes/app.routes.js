var utils = require('../../helpers/utils');
var multer = require('../../helpers/multer');
var resMsg = require('../../config/message');
var auth = require('../../config/auth');

/* Controller */
// var management_messages = require('../applications/management_messages/controllers/management_messages');
// var management_payment_channels = require('../applications/management_payments/controllers/management_payment_channels');
// var management_register = require('../applications/management_accounts/controllers/managements_register');
// var management_profile = require('../applications/management_accounts/controllers/management_profile');
var management_authentications = require('../applications/management_accounts/controllers/management_authentications');
//



var management_promotions = require('../applications/management_promotions/controllers/management_promotions');
// var management_stores = require('../applications/management_stores/controllers/management_stores');
// var management_stores_products = require('../applications/management_stores/controllers/management_stores_products');
// var management_stores_warehouses = require('../applications/management_stores/controllers/management_stores_warehouses');



var management_cars = require('../applications/management_configs/controllers/config_cartypes');
var management_job_services = require('../applications/management_types/controllers/management_job_services');
var management_users = require('../applications/management_types/controllers/management_users');

var management_dimensions = require('../applications/management_types/controllers/management_dimensions');

var passport = require('passport');

var GoogleTokenStrategy = require('passport-google-id-token');
var Infomation_Users = require('../applications/management_types/models/Infomation_users');



module.exports = function (app) {


    var path = '/api/v1/';
    /*----- Version Controll API true-----*/
    var management_versions  =  require('../applications/management_versions/controllers/management_versions');

    app.get(path + 'versions', management_versions.onVersions);
    app.get(path + 'version/:platform', management_versions.onVersions);
    app.post(path + 'version', management_authentications.checkTokenByUser, management_versions.onVersions_Create);
    app.put(path + 'version/:id',management_authentications.checkTokenByUser, management_versions.onVersions_Update);

    /*----- User Register Controll API -----*/
    var management_register = require('../applications/management_accounts/controllers/managements_register');

    app.get(path + 'register/check/username/:id', management_register.checkUsername);
    app.get(path + 'register/check/email/:id', management_register.checkEmail);
    app.get(path + 'register/check/msisdn/:id', management_register.checkMsisdn);
    app.get(path + 'register/check/facebook/:id', management_register.checkFacebook_id);
    app.get(path + 'register/check/google/:id', management_register.checkGoogle_id);

    app.post(path + 'register', management_register.onUpload_Images, management_register.onAccount_CheckRegister, management_register.onAccount_Creates);

    /*----- User Check Controll API -----*/

    app.get(path + 'find/userinfo/email/:id', management_authentications.checkTokenByUser, management_register.onFindEmail);


    /*----- User Verification Controll APIc -----*/
    var management_verifications = require('../applications/management_verifications/controllers/management_register_verifications');

    app.get(path + 'request/code/email/:id',  management_verifications.onRequest_CodeRegister);
    app.post(path + 'confirm/code/', management_verifications.onConfirm_CodeRegister);

    /*----- User Authetication Controll API -----*/
    var management_profile = require('../applications/management_accounts/controllers/management_profile');

    app.post(path + 'login/by/username', management_authentications.onAuthenByUserPass);
    app.put(path + 'notification', management_authentications.checkTokenByUser, management_profile.onProfileNotification_Update);


    app.post(path + 'login/by/google', passport.authenticate('google-id-token'), management_authentications.onAuthenByGoogle);

    /*----- User Profile Controll API -----*/
    app.put(path + 'change/password', management_authentications.checkTokenByUser, management_profile.resetpassword);
    app.put(path + 'change/profile', management_authentications.checkTokenByUser, management_profile.onUpload_Images, management_profile.onProfile_Update);
    app.get(path + 'profile', management_authentications.checkTokenByUser, management_profile.onProfiles);

    // app.put(path + 'change', management_profile.onProfile_Update);

    /*----- Job Sheet Controll API -----*/

    /*----- Google Distination Controll API -----*/


    /*----- Promotion Controll API -----*/
    app.post(path + 'promotions/create', management_authentications.checkTokenByUser, management_promotions.onCreatePromotion);
    app.put(path + 'promotions/update', management_authentications.checkTokenByUser, management_promotions.onUpload_Images, management_promotions.onUpdate_Promotion);
    app.get(path + 'promotions/check/:id', management_authentications.checkTokenByUser, management_promotions.onCheckPromotion_Code);
    app.put(path + 'promotions/usage/:id', management_authentications.checkTokenByUser, management_promotions.onUsePromotion_Code);
    app.get(path + 'promotions/check/discount/:id', management_authentications.checkTokenByUser, management_promotions.onCheckPromotion_Discount);

    /*----- Payment Channel Controll API -----*/
    var management_payment_channels = require('../applications/management_payments/controllers/management_payment_channels');

    app.get(path +'price/:id', management_authentications.checkTokenByUser, management_payment_channels.onPrices);
    app.post(path +'price', management_authentications.checkTokenByUser, management_payment_channels.onCreatePrices);
    app.put(path +'price/:id', management_authentications.checkTokenByUser, management_payment_channels.onUpdatePrices);

    app.put(path +'price/:id/add/type', management_authentications.checkTokenByUser, management_payment_channels.onAddPrices_TypeByCompany);
    app.put(path +'price/update/type/:id', management_authentications.checkTokenByUser, management_payment_channels.onUpdatePrices_TypeByCompany);
    app.delete(path +'price/delete/type/:id', management_authentications.checkTokenByUser, management_payment_channels.onDeletePrices_TypeByCompany);

    /*----- Payment Controll API -----*/


    /*----- Response Message Controll API -----*/
    var management_messages = require('../applications/management_messages/controllers/management_messages');

    app.get(path + 'messages', management_messages.onMessages);
    app.get(path + 'message/:id', management_messages.onMessages);
    app.post(path + 'message', management_messages.onMessage_Create);
    app.put(path + 'message/:id', management_messages.onMessage_Update);

    /*----- Company Controll API -----*/
    var management_companys = require('../applications/management_companys/controllers/management_companys');

    app.get(path + 'company/:user', management_authentications.checkTokenByUser, management_companys.onCompanyByUser);
    app.get(path + 'companys', management_authentications.checkTokenByUser, management_companys.onCompanys);
    app.get(path + 'companys/:id', management_authentications.checkTokenByUser, management_companys.onCompanys);
    app.get(path + 'companys/:id/:user', management_authentications.checkTokenByUser, management_companys.onCompanys);

    app.post(path + 'companys', management_authentications.checkTokenByUser, management_companys.onCompanys_Create);
    app.put(path + 'companys/:id/logo', management_authentications.checkTokenByUser, management_companys.onCompanys);
    app.put(path + 'companys/:id', management_authentications.checkTokenByUser, management_companys.onCompanys_Update);
    app.put(path + 'companys/:id/users', management_authentications.checkTokenByUser, management_companys.onCompanys_Add_User, management_companys.onCompanys_Add_UserSendEmail);
    app.delete(path + 'companys/:id/users', management_authentications.checkTokenByUser, management_companys.onCompanys_Delete_User);

    /*----- Stores Controll API -----*/
    // app.get(path + 'store/:id', management_authentications.checkTokenByUser, management_stores.onStores);
    // app.post(path + 'store', management_authentications.checkTokenByUser, management_stores.onStores_Import);
    // app.put(path + 'store/:id', management_authentications.checkTokenByUser, management_stores.onStores_Update);
    // app.delete(path + 'store/:id', management_authentications.checkTokenByUser, management_stores.onStores_Export);

    /*----- Stores Products Infomation Controll API -----*/
    // app.get(path + 'products', management_authentications.checkTokenByUser, management_stores_products.onStore_Products);
    // app.get(path + 'products/:id', management_authentications.checkTokenByUser, management_stores_products.onStore_Products);
    // app.post(path + 'product', management_authentications.checkTokenByUser, management_stores_products.onUpload_Images, management_stores_products.onStores_Product_Import);
    // app.put(path + 'product/:id', management_authentications.checkTokenByUser, management_stores_products.onUpload_Images, management_stores_products.onStores_Product_Update);



    /*----- Management Type Controll API -----*/
    // app.get(path + 'management/type/cars', management_authentications.checkTokenByUser, management_cars.onQuery_Cars);
    // app.get(path + 'management/type/cars/:id', management_authentications.checkTokenByUser, management_cars.onQuery_Cars);
    // app.post(path + 'management/type/cars', management_authentications.checkTokenByUser, management_cars.onCreate_Cars);
    // app.put(path + 'management/type/cars/:id', management_authentications.checkTokenByUser, management_cars.onUpdate_Cars);

    app.get(path + 'management/dimensions/jobs', management_authentications.checkTokenByUser, management_dimensions.onQuery_Dimensions);
    app.get(path + 'management/dimensions/jobs/:id', management_authentications.checkTokenByUser, management_dimensions.onQuery_Dimensions);
    app.post(path + 'management/dimensions/jobs', management_authentications.checkTokenByUser, management_dimensions.onCreate_Dimensions);
    app.put(path + 'management/dimensions/jobs/:id', management_authentications.checkTokenByUser, management_dimensions.onUpdate_Dimensions);

    app.post(path + 'jobss',  management_dimensions.onCheckDimenPrice);


    // app.get(path + 'management/service/jobs', management_authentications.checkTokenByUser, management_job_services.onQuery_JosServices);
    // app.get(path + 'management/service/jobs/:id', management_authentications.checkTokenByUser, management_job_services.onQuery_JosServices);
    // app.post(path + 'management/service/jobs', management_authentications.checkTokenByUser, management_job_services.onCreate_JosServices);
    // app.put(path + 'management/service/jobs/:id', management_authentications.checkTokenByUser, management_job_services.onUpdate_JosServices);

    // app.get(path + 'management/type/users', management_authentications.checkTokenByUser, management_users.onQuery_Users);
    // app.post(path + 'management/type/users', management_authentications.checkTokenByUser, management_users.onCreate_Users);
    // app.put(path + 'management/type/users/:id', management_authentications.checkTokenByUser, management_users.onUpdate_Users);

    /*----- Config Controll API Support CarTypes-----*/
    var config_cartypes = require('../applications/management_configs/controllers/config_cartypes');

    app.get(path + 'config/type/cars', management_authentications.checkTokenByUser, config_cartypes.onQuery_Cars);
    app.get(path + 'config/type/cars/:id', management_authentications.checkTokenByUser, config_cartypes.onQuery_Cars);
    app.post(path + 'config/type/cars', management_authentications.checkTokenByUser, config_cartypes.onCreate_Cars);
    app.put(path + 'config/type/cars/:id', management_authentications.checkTokenByUser, config_cartypes.onUpdate_Cars);

    app.put(path + 'config/type/cars/dimension/:id', management_authentications.checkTokenByUser, config_cartypes.onUpdateConfigsSupportDimension);
    app.delete(path + 'config/type/cars/dimension/:id', management_authentications.checkTokenByUser, config_cartypes.onDeleteConfigsSupportDimension);

    app.put(path + 'config/type/cars/distances/:id', management_authentications.checkTokenByUser, config_cartypes.onUpdateConfigsSupportDistances);
    app.delete(path + 'config/type/cars/distances/:id', management_authentications.checkTokenByUser, config_cartypes.onDeleteConfigsSupportDistances);

    /*----- Config Controll API Support JobTypes-----*/
    var config_jobtypes = require('../applications/management_configs/controllers/config_jobtypes');

    app.get(path + 'config/type/jobs', management_authentications.checkTokenByUser, config_jobtypes.onQuery_Jobs);
    app.get(path + 'config/type/jobs/:id', management_authentications.checkTokenByUser, config_jobtypes.onQuery_Jobs);
    app.post(path + 'config/type/jobs', management_authentications.checkTokenByUser, config_jobtypes.onCreate_Jobs);
    app.put(path + 'config/type/jobs/:id', management_authentications.checkTokenByUser, config_jobtypes.onUpdate_Jobs);

    /*----- Config Controll API Support UserTypes-----*/
    var config_usertypes = require('../applications/management_configs/controllers/config_usertypes');

    app.get(path + 'config/type/users', management_authentications.checkTokenByUser, config_usertypes.onQuery_UserTypes);
    app.get(path + 'config/type/user/:id', management_authentications.checkTokenByUser, config_usertypes.onQuery_UserType);
    app.post(path + 'config/type/user', management_authentications.checkTokenByUser, config_usertypes.onCreate_UserTypes);
    app.put(path + 'config/type/user/:id', management_authentications.checkTokenByUser, config_usertypes.onUpdate_UserTypes);

    /*----- Partners Controll API Support WareHouse-----*/
    var management_partners = require('../applications/management_partner/controllers/management_partners');

    app.get(path + 'partners', management_authentications.checkTokenByUser, management_partners.onPartners);
    app.get(path + 'partner/:id', management_authentications.checkTokenByUser, management_partners.onPartner);
    app.get(path + 'partner/company/:id', management_authentications.checkTokenByUser, management_partners.onPartners);

    app.post(path + 'partner', management_authentications.checkTokenByUser, management_partners.onPartners_Create);

    app.put(path + 'partner/:id', management_authentications.checkTokenByUser, management_partners.onPartners_Update);

    /*----- Stores warehouse Controll API Support WareHouse-----*/
    var management_warehouses = require('../applications/management_warehouses/controllers/management_warehouses');

    app.get(path + 'warehouses', management_authentications.checkTokenByUser, management_warehouses.onQuery_Warehouses);
    app.get(path + 'warehouses/:partners', management_authentications.checkTokenByUser, management_warehouses.onQuery_Warehouses);
    app.get(path + 'warehouse/:id', management_authentications.checkTokenByUser, management_warehouses.onQuery_WarehouseById);
    app.get(path + 'warehouse/company/:id', management_authentications.checkTokenByUser, management_warehouses.onQuery_WarehouseByCompanyId);
    app.post(path + 'warehouse', management_authentications.checkTokenByUser, management_warehouses.onCreate_Warehouse);
    app.put(path + 'warehouse/:id', management_authentications.checkTokenByUser, management_warehouses.onUpdate_Warehouse);
    app.put(path + 'warehouse/:id/support/company', management_authentications.checkTokenByUser, management_warehouses.onWarehouse_AddCompanyById);
    app.delete(path + 'warehouse/:id/support/company', management_authentications.checkTokenByUser, management_warehouses.onWarehouse_DeleteCompanyById);

    /*----- Management Type Products API Support WareHouse-----*/
    var management_product_type = require('../applications/management_warehouses/controllers/management_product_types');

    app.get(path + 'management/product/types', management_authentications.checkTokenByUser, management_product_type.onQuery_Product_Types);
    app.get(path + 'management/product/types/:id', management_authentications.checkTokenByUser, management_product_type.onQuery_Product_Type);
    app.post(path + 'management/product/types', management_authentications.checkTokenByUser, management_product_type.onCreate_Product);
    app.put(path + 'management/product/types/:id', management_authentications.checkTokenByUser, management_product_type.onUpdate_Product);

    /*----- Management Type Products API Support WareHouse-----*/
    var management_product_serials = require('../applications/management_warehouses/controllers/management_product_serials');

    app.get(path + 'management/product/serials', management_authentications.checkTokenByUser, management_product_serials.onQuery_Product_Serials);
    app.get(path + 'management/product/serials/:partners', management_authentications.checkTokenByUser, management_product_serials.onQuery_Product_SerialByPartners);

    app.get(path + 'management/product/serial/:id', management_authentications.checkTokenByUser, management_product_serials.onQuery_Product_Serial);
    app.get(path + 'management/product/serial/:id/:partners', management_authentications.checkTokenByUser, management_product_serials.onQuery_Product_SerialAndPartners);


    app.post(path + 'management/product/serial', management_product_serials.onUpload_Images, management_authentications.checkTokenByUser, management_product_serials.onCreate_Product_Serial);
    app.put(path + 'management/product/serial/:id', management_authentications.checkTokenByUser, management_product_serials.onUpload_Images, management_product_serials.onUpdate_Product_Serial);
    app.put(path + 'management/product/serial/:id/support', management_authentications.checkTokenByUser, management_product_serials.onProduct_Serial_AddSupport);
    app.delete(path + 'management/product/serial/:id/support', management_authentications.checkTokenByUser, management_product_serials.onProduct_Serial_DeleteSupport);

    /*----- Management Store Product API Support WareHouse-----*/
    var management_store_products = require('../applications/management_warehouses/controllers/management_store_products');

    app.get(path + 'management/product/store/:id', management_authentications.checkTokenByUser, management_store_products.onQuery_Store_Product);

    app.get(path + 'management/product/stores', management_authentications.checkTokenByUser, management_store_products.onQuery_Store_Products);
    app.get(path + 'management/product/stores/:company', management_authentications.checkTokenByUser, management_store_products.onQuery_Store_Products);
    app.get(path + 'management/product/stores/:company/:warehouse', management_authentications.checkTokenByUser, management_store_products.onQuery_Store_Products);
    app.get(path + 'management/product/stores/:company/:warehouse/:partner', management_authentications.checkTokenByUser, management_store_products.onQuery_Store_Products);

    app.get(path + 'management/product/warehouse/:warehouse', management_authentications.checkTokenByUser, management_store_products.onQuery_Store_ProductByWarehouses);
    app.get(path + 'management/product/partner/import/:id', management_authentications.checkTokenByUser, management_store_products.onQuery_Store_ProductByPartners);
    app.get(path + 'management/product/partner/export/:id', management_authentications.checkTokenByUser, management_store_products.onQuery_Store_ProductByPartnersExport);


    app.get(path + 'management/product/count/:partner', management_authentications.checkTokenByUser, management_store_products.onQuery_Store_ProductBycompanyAndwarehouseSum);
    app.get(path + 'management/product/count/:company/:warehouse', management_authentications.checkTokenByUser, management_store_products.onQuery_Store_ProductBycompanyAndwarehouseSum);
    app.get(path + 'management/product/count/:company/:warehouse/:partner', management_authentications.checkTokenByUser, management_store_products.onQuery_Store_ProductBycompanyAndwarehouseSum);

    app.post(path + 'management/product/store/import', management_authentications.checkTokenByUser, management_warehouses.onCheck_WarehouseSupport, management_product_serials.onCheck_ProductSerialSupport, management_store_products.onCreate_Store_Products);
    app.put(path + 'management/product/store/export/:id', management_authentications.checkTokenByUser, management_store_products.onExport_Store_Products);
    // app.put(path + 'management/product/store/:id', management_authentications.checkTokenByUser, management_warehouses.onCheck_WarehouseSupport);
    // app.put(path + 'management/product/store/:id/support', management_authentications.checkTokenByUser, management_store_products.onProduct_Serial_AddSupport);
    // app.delete(path + 'management/product/store/:id/support', management_authentications.checkTokenByUser, management_store_products.onProduct_Serial_DeleteSupport);


    // /*----- JobSheets Controll API -----*/
    //
    // var management_jobsheets = require('../applications/management_jobsheets/controllers/management_jobsheets');
    //
    // app.get(path + 'jobsheets', management_authentications.checkTokenByUser, management_jobsheets.onQueryJobs);
    // app.get(path + 'jobsheets/:id', management_authentications.checkTokenByUser, management_jobsheets.onQueryJobs);
    // app.post(path + 'jobsheets', management_authentications.checkTokenByUser, management_jobsheets.onSumDimension, management_jobsheets.onCreateJobs);
    // app.put(path + 'jobsheets/:id', management_authentications.checkTokenByUser, management_jobsheets.onSumDimension, management_jobsheets.on๊UpdateJobs);


    /*-----  Controll API -----*/

    var management_transportaions = require('../applications/management_transportations/controllers/management_transportaions');

    app.get(path + 'transportaions', management_authentications.checkTokenByUser, management_transportaions.onTransportations);
    app.get(path + 'transportaions/:id', management_authentications.checkTokenByUser, management_transportaions.onTransportationsById);

    app.get(path + 'location/transportaions/:id', management_authentications.checkTokenByUser, management_transportaions.onLocationstransportationsById);
    app.put(path + 'location/transportaions/:id', management_authentications.checkTokenByUser, management_transportaions.onUpdateLocationstransportationsById);

    app.post(path + 'transportaions', management_authentications.checkTokenByUser, management_transportaions.onSumDimension, config_cartypes.onQuery_CarsType,  management_transportaions.onCreateTransportaions);
    app.put(path + 'transportaions/:id', management_authentications.checkTokenByUser, management_transportaions.onSumDimension, management_transportaions.on๊UpdateTransportaions);


    // app.post(path + 'configs/:id', management_authentications.checkTokenByUser, config_cartypes.onQuery_CarsType);

    // var management_transportaions_config = require('../applications/management_transportations/controllers/management_transportaions_config');

    // app.get(path + 'config/transportaions', management_authentications.checkTokenByUser, management_transportaions_config.onConfigsAll);
    // app.get(path + 'config/transportaions/company/:id', management_authentications.checkTokenByUser, management_transportaions_config.onConfigByCompany);
    // app.post(path + 'config/transportaions/company', management_authentications.checkTokenByUser, management_transportaions_config.onCreateConfigs);
    //
    //
    // app.put(path + 'config/transportaions/partner/dimension/:id', management_authentications.checkTokenByUser, management_transportaions_config.onUpdateConfigsSupportDimension);
    // app.delete(path + 'config/transportaions/partner/dimension/:id', management_authentications.checkTokenByUser, management_transportaions_config.onDeleteConfigsSupportDimension);
    //
    // app.put(path + 'config/transportaions/partner/distances/:id', management_authentications.checkTokenByUser, management_transportaions_config.onUpdateConfigsSupportDistances);
    // app.delete(path + 'config/transportaions/partner/distances/:id', management_authentications.checkTokenByUser, management_transportaions_config.onDeleteConfigsSupportDistances);




    // app.get(path + 'transportaions/:id', management_authentications.checkTokenByUser, management_transportaions.onQueryJobs);
    // app.post(path + 'transportaions', management_authentications.checkTokenByUser, management_jobsheets.onSumDimension, management_jobsheets.onCreateJobs);
    // app.put(path + 'transportaions/:id', management_authentications.checkTokenByUser, management_jobsheets.onSumDimension, management_jobsheets.on๊UpdateJobs);







    passport.serializeUser(function(user, done) {
        done(null, user);
    });

    passport.deserializeUser(function(obj, done) {
        done(null, obj);
    });

    passport.use(new GoogleTokenStrategy({
            // clientID: '407408718192.apps.googleusercontent.com'
            clientID: auth.googleAuth.clientID
            // getGoogleCerts: optionalCustomGetGoogleCerts
        },
        function(parsedToken, googleId, done) {
            return done(null,{ googleId: googleId });
        }

    ));
    app.post('/auth/google', passport.authenticate('google-id-token'),
        function (req, res) {
            console.log(req);
            // do something with req.user
            res.send(req.user? 200 : 401);
        }
    );

    /*----- Catch 404 Error -----*/
    app.use(function (req, res) {
        res.status(404).json(resMsg.getMsg(40400));
        utils.writeLog('Client Request', req, utils.getTimeInMsec(), '----- Request Error URL[' + req.url + '] Status[' + JSON.stringify(resMsg.getMsg(40400)) + '] -----');
    });

    /*----- Catch 500 Error -----*/
    app.use(function (err, req, res) {
        console.log(err);
        res.status(501).json(resMsg.getMsg(50001));
        utils.writeLog('Client Request', req, utils.getTimeInMsec(), '----- Request Error URL[' + req.url + '] Status[' + JSON.stringify(resMsg.getMsg(50001)) + '] -----', err);
    });

};

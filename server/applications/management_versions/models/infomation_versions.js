'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Infomation_VersionsSchema  = new Schema({
    message             : {type: String, default: null},
    platform            : {type: String, default: null},
    appId               : {type: String, default: null},
    redirectUrl         : {type: String, default: null},
    version             : {type: Number, default: null},
    enable              : {type: Boolean, default: true}
});

var Infomation_Versions = mongoose.model('Infomation_Versions', Infomation_VersionsSchema, 'Infomation_Versions');
module.exports = Infomation_Versions;





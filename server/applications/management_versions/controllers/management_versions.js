var utils = require('../../../../helpers/utils');
var resMsgs = require('../../management_messages/controllers/management_messages');
var mongo = require('mongodb');

var Infomation_Versions = require('../../management_versions/models/infomation_versions');

exports.s = function (request, response, next) {

    console.log(1);
    console.log(request.googleId);

    // resMsgs.onMessage_Response(0, 40401, function (res) {
    //     response.status(404).json(res);
    //     utils.writeLog(command, request, startTime, res, err)
    // });
}

exports.onVersions = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onVersions";
    try {
        var query = {};
        // (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;
        (request.params.platform != null) ? query.platform = request.params.platform : null;

        Infomation_Versions
            .find(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40401, function (res) {
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })
    }catch (err) {
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.onVersions_Create = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onVersions_Create";
    try {

        var create = {};
        // (request.body.server.profile_id != null) ? create.by = request.body.server.profile_id : null;
        create.date = startTime;

        var data = {};
        (request.body.message != null) ? data.message = request.body.message : null;
        (request.body.platform != null) ? data.platform = request.body.platform : null;
        (request.body.appId != null) ? data.appId = request.body.appId : null;
        (request.body.redirectUrl != null) ? data.redirectUrl = request.body.redirectUrl : null;
        (request.body.version != null) ? data.version = request.body.version : null;
        (request.body.enable != null) ? data.enable = request.body.enable : null;

        data.create = create;

        var infomation_versions = new Infomation_Versions(data);
        infomation_versions.save(function (err, doc) {
            if(!err){
                resMsgs.onMessage_Response(0,20000,function(res){
                    var resData = res;
                    resData.data = doc;
                    response.status(200).json(resData);
                    utils.writeLog(command, request, startTime, res, err)
                });
            }else{
                resMsgs.onMessage_Response(0,50003,function(res){
                    response.status(500).json(res);
                    utils.writeLog(command, request, startTime, res, err)
                });
            }
        });

    }catch (err) {
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}


exports.onVersions_Update = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onVersions_Create";
    try {
        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        var data = {};
        (request.body.message != null) ? data.message = request.body.message : null;
        (request.body.platform != null) ? data.platform = request.body.platform : null;
        (request.body.appId != null) ? data.appId = request.body.appId : null;
        (request.body.redirectUrl != null) ? data.redirectUrl = request.body.redirectUrl : null;
        (request.body.version != null) ? data.version = request.body.version : null;
        (request.body.enable != null) ? data.enable = request.body.enable : null;

        // var update = {};
        // (request.body.server_by != null)? update.by = request.body.server_by : null;
        // update.date = startTime;
        //
        // var push = {};
        // push.update =  update;
        // data.$push = push;


        console.log(query);
        console.log(data);

        Infomation_Versions
            .findOneAndUpdate(query,data, {new:true})
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    console.log(err);
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40401, function (res) {
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })

    }catch (err) {
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}
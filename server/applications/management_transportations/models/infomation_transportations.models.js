'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Infomation_TransportationsSchema  = new Schema({
    company             : {type: Schema.ObjectId, ref: 'Infomation_Companys'},
    partner             : {type: Schema.ObjectId, ref: 'Infomation_Partners'},
    ref                 : [{
        title           : {type: String, default: null},
        code            : {type: String, default: null},
        description     : {type: String, default: null},
    }],
    cars                : [{type: Schema.ObjectId, ref: 'Config_CarTypes'}],
    type                : {
        name            : {type: String, default:null},
        key             : {type: Number, default: 0.0},
        description     : {type: String, default: null},
    },  // roundtrip, reservation, install
    total_distance      : {type: Number, default:0.0},
    total_price         : {type: Number, default:0.0},
    total_dimension     : {
        weight          : {type: Number, default:0.0},
        long            : {type: Number, default:0.0},
        height          : {type: Number, default:0.0},
    },
    promotions          : {type: Schema.ObjectId, ref: 'Management_Promotions_Card'},
    payment_channel     : {type: Schema.ObjectId, ref: 'Payment_Channels'},

    start               : {type: Date, default:null},
    enable              : {type: Boolean, default: true},
    transportation_position           : [{
        title           : {type: String, default:null},
        description     : {type: String, default: null},
        contact         : {
            name        : {type: String, default:null},
            msisdn      : {type: String, default:null},
            note        : {type: String, default:null},
        },
        locations       : {
            address     : {type: String, default:null},
            lat         : {type: String, default:0.0},
            long        : {type: String, default:0.0},
        },
        addon           : [{type: Schema.ObjectId, ref: 'Job_Services'}],
        controll        : {
            start       : {type: Date, default:null},
            end         : {type: Date, default:null},
            images      : [{
                image   : {type: String, default:null},
            }],
            signature   : {type: String, default:null},
            note        : {type: String, default:null},
            location    : {
                lat     : {type: Number, default:0.0},
                long    : {type: Number, default:0.0},
            }
        },
        products        : [{
            tag_code    : {type: Schema.ObjectId, ref: 'Infomation_Store_Products'},
            checker     : {type: Boolean, default:false},
        }],
    }],

    location           : [{
        lat             : {type: Number, default:0.0},
        long            : {type: Number, default:0.0},
    }],

    // benefit             : {
    //     total           : {type: Number, default:0.0},
    //     companey        : {type: Number, default:0.0},
    //     expense         : {type: Number, default:0.0},
    //     system          : {type: Number, default:0.0}
    // },

    create              : {
        by              : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date            : {type: Date, default:  Date.now}
    },
    update              : [{
        by              : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date            : {type: Date, default:  Date.now}
    }]

});

var Infomation_Transportations = mongoose.model('Infomation_Transportations', Infomation_TransportationsSchema, 'Infomation_Transportations');
module.exports = Infomation_Transportations;
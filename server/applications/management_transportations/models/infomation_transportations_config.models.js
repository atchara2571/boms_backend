'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Infomation_transportation_configsSchema  = new Schema({
    company             : {type: Schema.ObjectId, ref: 'Infomation_Companys'},
    support             : [{
        partner         : {type: Schema.ObjectId, ref: 'Infomation_Partners'},
        distances       : [{
            number      : {type: Number, default:0.0},
            price       : {type: Number, default:0.0},
        }],
        dimension       : [{
            weight      : {type: Number, default:0.0},
            long        : {type: Number, default:0.0},
            height      : {type: Number, default:0.0},
        }]

    }]
});

var infomation_transportation_configs = mongoose.model('Infomation_Transportation_Configs', Infomation_transportation_configsSchema, 'Infomation_Transportation_Configs');
module.exports = infomation_transportation_configs;
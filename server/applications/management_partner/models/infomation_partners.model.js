'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Infomation_PartnersSchema  = new Schema({
    company             : {type: Schema.ObjectId, ref: 'Infomation_Companys'},
    title               : {type: String, default: null},
    contact             : {
        name            : {type: String, default:null},
        msisdn          : {type: String, default:null},
        email            : {type: String, default:null},
        address         : {type: String, default:null},
        lat             : {type: Number, default:0.0},
        long            : {type: Number, default:0.0}
    },
    invoice             : [],
    enable              : {type: Boolean, default: true},
    users               : [{
        by              : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        type            : {type: Schema.ObjectId, ref: 'Config_UserTypes'},
    }],
    create              : {
        by              : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date            : {type: Date, default:  Date.now}
    },
    update              : [{
        by              : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date            : {type: Date, default:  Date.now}
    }]
});

var Infomation_Partners = mongoose.model('Infomation_Partners', Infomation_PartnersSchema, 'Infomation_Partners');
module.exports = Infomation_Partners;
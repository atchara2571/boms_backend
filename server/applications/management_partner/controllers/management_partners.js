var utils = require('../../../../helpers/utils');
var resMsgs = require('../../management_messages/controllers/management_messages');
var mongo = require('mongodb');

var infomation_partners = require('../models/infomation_partners.model');

exports.onPartners = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onPartners";
    try {
        var query = {};

        infomation_partners
            .find(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,50003,function(res){
                            response.status(500).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onPartner = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onPartners";
    try {
        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        infomation_partners
            .findOne(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,50003,function(res){
                            response.status(500).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onPartners_Company = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onPartners";
    try {
        var query = {};
        (request.params.id != null) ? query.companey = new mongo.ObjectID(request.params.id) : null;

        infomation_partners
            .find(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,50003,function(res){
                            response.status(500).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onPartners_Create = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onPartners_Create";
    try {
        var create = {};
        (request.body.server_by != null)? create.by = request.body.server_by : null;
        create.date = startTime;

        var data = {};
        (request.body.title != null)? data.title = request.body.title : null;
        (request.body.company != null)? data.company = new mongo.ObjectID(request.body.company) : null;
        (request.body.contact != null)? data.contact = request.body.contact : null;
        (request.body.invoice != null)? data.invoice = request.body.invoice : null;
        (request.body.users != null)? data.users = request.body.users : null;
        (request.body.enable != null)? data.enable = request.body.enable : null;
        data.create = create;

        var infomation_partner = new infomation_partners(data);
        infomation_partner.save(function (err, doc) {
            delete doc.create;
            delete doc.update;
            if (!err) {
                resMsgs.onMessage_Response(0,20000,function(res){
                    var resData = res;
                    resData.data = doc;
                    response.status(200).json(resData);
                    utils.writeLog(command, request, startTime, res, err)
                });
            } else {
                resMsgs.onMessage_Response(0,50003,function(res){
                    response.status(500).json(res);
                    utils.writeLog(command, request, startTime, res, err)
                });
            }
        });

    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onPartners_Update = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onPartners_Update";
    try {
        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        var data = {};
        (request.body.title != null)? data.title = request.body.title : null;
        (request.body.company != null)? data.company = new mongo.ObjectID(request.body.company) : null;
        (request.body.contact != null)? data.contact = request.body.contact : null;
        (request.body.invoice != null)? data.invoice = request.body.invoice : null;
        (request.body.users != null)? data.users = request.body.users : null;
        (request.body.enable != null)? data.enable = request.body.enable : null;

        var update = {};
        (request.body.server_by != null)? update.by = request.body.server_by : null;
        update.date = startTime;

        var push = {};
        push.update =  update;
        data.$push = push;

        infomation_partners
            .findOneAndUpdate(query,data,{new:true})
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,50003,function(res){
                            response.status(500).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });

    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};
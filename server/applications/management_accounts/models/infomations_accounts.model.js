'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Infomation_AccountsSchema  = new Schema({

    username                : {type: String, required: true, unique: true},
    password                : {type: String, required: true },
    // usertype_id             : [{type: Schema.ObjectId, ref: 'Infomation_UserTypes'}],

    x_access_token          : {type: String, default: null},
    token_expired           : {type: Number, default: 0},
    status                  : {type: Number, default: 0}, // 0 = reserve , 1 = enable , 2 = disable
    state                   : {type: Number, default: 0}, // 0 = logout, 1 = login
    last_login              : {type: Date, default:  Date.now},

    userinfo                : {
        firstname           : {type: String, default: null},
        lastname            : {type: String, default: null},
        image_profile       : {type: String, default: null},
        card_id             : {type: String, default: null},
        birthday            : {type: Date, required: true},
        msisdn              : {type: String, required: true, unique: true},
        email               : {type: String, required: true, unique: true},
        lineid              : {type: String, default: null},
        facebook_id         : {type: String, default: null},
        google_id           : {type: String, default: null},
        address: {
            address         : {type: String, default: null},
            zipcode         : {type: String, default: null}
        }
    },
    // notification            : {
    //     platform            : {type: String, default: null},
    //     pns_token           : {type: String, default: null},
    //     pns_key             : {type: String, default: null},
    //     pns_packge          : {type: String, default: null},
    //     enable              : {type: Boolean, default: true}
    // },
    enable                  : {type: Boolean, default: true}


});

var Infomation_Accounts = mongoose.model('Infomation_Accounts', Infomation_AccountsSchema, 'Infomation_Accounts');
module.exports = Infomation_Accounts;

'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Infomations_UsersSchema  = new Schema({
    company             : {type: Schema.ObjectId, ref: 'Infomation_Companys'},
    title               : {type: String, default:null},
    key                 : {type: Number, default: null},
    description         : {type: String, default: null},
    enable              : {type: Boolean, default: true},
    create              : {
        by              : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date            : {type: Date, default:  Date.now}
    },
    update              : [{
        by              : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date            : {type: Date, default:  Date.now}
    }]
});

var Infomations_Users = mongoose.model('Infomations_Users', Infomations_UsersSchema, 'Infomations_Users');
module.exports = Infomations_Users;
'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Infomations_JobServicesSchema  = new Schema({
    company             : {type: Schema.ObjectId, ref: 'Infomation_Companys'},
    title               : {type: String, default:null},
    price               : {type: Number, default: null},
    description         : {type: String, default: null},
    enable              : {type: Boolean, default: true},
    create              : {
        by              : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date            : {type: Date, required: true}
    },
    update              : [{
        by              : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date            : {type: Date, required: true}
    }]
});

var Infomations_JobServices = mongoose.model('Infomations_JobServices', Infomations_JobServicesSchema, 'Infomations_JobServices');
module.exports = Infomations_JobServices;
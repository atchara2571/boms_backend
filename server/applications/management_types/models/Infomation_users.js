'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Infomation_UsersSchema  = new Schema({
    googleId            : {type: String, default:null},
    description         : {type: String, default: null},
    enable              : {type: Boolean, default: true}
});

var Infomation_Users = mongoose.model('Infomation_Users', Infomation_UsersSchema, 'Infomation_Users');
module.exports = Infomation_Users;
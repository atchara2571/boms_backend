var utils = require('../../../../helpers/utils');
var configs = require('../../../../config/config');
var resMsgs = require('../../management_messages/controllers/management_messages');
var mongo = require('mongodb');

var infomations_accounts = require('../../management_accounts/models/infomations_accounts.model');

var infomation_verifications = require('../models/infomation_verifications');


exports.onTransactionExpired = function (request,response,next) {
    var command = 'onConfirm_CodeRegister';
    var startTime = utils.getTimeInMsec();

    var query = {};
    (request.params.id != null) ? query['userinfo.email'] = request.params.id : null;

    infomation_verifications
        .findOne(query)
        .lean()
        .exec(function (err, doc) {
            if(doc != null){
                if(doc.transactionExpired > utils.getTimeInMsec()){
                    resMsgs.onMessage_Create(0,40108,function(res){
                        response.status(401).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                }else{
                    next();
                }
            }else{
                resMsgs.onMessage_Create(0,40300,function(res){
                    response.status(403).json(res);
                    utils.writeLog(command, request, startTime, res, err)
                });
            }
        });

}

exports.onRequest_CodeRegister = function (request,response) {

    var command = 'onRequest_CodeRegister';
    var startTime = utils.getTimeInMsec();

    var query = {};
    (request.params.id != null) ? query['userinfo.email'] = request.params.id : null;

    infomations_accounts
        .findOne(query)
        .lean()
        .exec(function (err, doc){
            delete doc.password;
            delete doc.usertype_id;
            delete doc.x_access_token;
            delete doc.token_expired;
            delete doc.status;
            delete doc.state;
            delete doc.last_login;
            delete doc.notification;
            delete doc.enable;

            if(doc != null){
                utils.createTransaction(function (err, token) {
                    if (!err){

                        var querys = {};
                        querys.email          = request.params.id;

                        var update = {};
                        update.transaction = token;
                        update.transactionExpired = utils.getTimeInMsec() + configs.transactionExpired;

                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            delete update.otp;
                            resData.data = update;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, resData, err)
                        });

                        update.type = 0;
                        update.username        = doc.username;
                        update.email           = request.params.id;


                        infomation_verifications
                            .findOne(querys)
                            .lean()
                            .exec(function (err, docs) {
                                update.otp = utils.randomNumber(4);
                                if(docs != null){
                                    infomation_verifications
                                        .findOneAndUpdate(querys, update, { new: true })
                                        .lean()
                                        .exec(function (err, docs) {
                                            utils.sendEmail(command,docs.otp,querys.email,"http://",function (error, info) {} );
                                            // utils.sendSms(docs.otp,request.body.mobile);
                                        });
                                }else{

                                    var infomation_verification = new infomation_verifications(update);
                                    infomation_verification.save(function (err, doc_otp) {
                                        utils.sendEmail(command,doc_otp.otp,querys.email,"http://",function (error, info) {} );
                                        // utils.sendSms(doc_otp.otp,request.body.mobile);
                                    });
                                }
                            });

                    }
                });

            }else{
                resMsgs.onMessage_Response(0,40104,function(res){
                    response.status(401).json(res);
                    utils.writeLog(command, request, startTime, res, err)
                });
            }
        })

}


exports.onConfirm_CodeRegister = function (request,response,next) {
    var command = 'onConfirm_CodeRegister';
    var startTime = utils.getTimeInMsec();

    var query_otp = {};
    query_otp.username = request.body.username;
    query_otp.otp = request.body.otp;
    query_otp.transaction = request.body.transaction;
    query_otp.type = 0;

    infomation_verifications
        .findOne(query_otp)
        .lean()
        .exec(function (err, doc) {
            if(doc != null){
                if(doc.transactionExpired > utils.getTimeInMsec()){
                    resMsgs.onMessage_Response(0,20000,function(res){
                        var resData = res;
                        // resData.data = data;


                        infomation_verifications
                            .findOneAndRemove(query_otp)
                            .lean()
                            .exec(function (err,docs) {
                            });

                        var query = {};
                        query.username = request.body.username;
                        var update = {};
                        update.status = 1;
                        infomations_accounts
                            .findOneAndUpdate(query, update, { new: true })
                            .lean()
                            .exec(function (err, doc_user) {

                            });
                        response.status(200).json(resData);
                        utils.writeLog(command, request, startTime, resData, err);
                    });
                }else{
                    resMsgs.onResponse_Messages(0,40102,function(res){
                        response.status(401).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                }
            }else{
                resMsgs.onResponse_Messages(0,40300,function(res){
                    response.status(403).json(res);
                    utils.writeLog(command, request, startTime, res, err)
                });
            }
        });

}
'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Config_UserTypesSchema  = new Schema({
    title               : {type: String, default:null},
    key                 : {type: Number, default: null, unique: true},
    description         : {type: String, default: null},
    enable              : {type: Boolean, default: true},
    create              : {
        by              : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date            : {type: Date, default:  Date.now}
    },
    update              : [{
        by              : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date            : {type: Date, default:  Date.now}
    }]
});

var Config_UserTypes = mongoose.model('Config_UserTypes', Config_UserTypesSchema, 'Config_UserTypes');
module.exports = Config_UserTypes;
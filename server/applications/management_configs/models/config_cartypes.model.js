'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Config_CarTypesSchema  = new Schema({
    company             : {type: Schema.ObjectId, ref: 'Infomation_Companys'},
    title               : {type: String, default:null},
    description         : {type: String, default: null},
    enable              : {type: Boolean, default: true},
    support             : [{
        partner         : {type: Schema.ObjectId, ref: 'Infomation_Partners'},
        distances       : [{
            number      : {type: Number, default:0.0},
            price       : {type: Number, default:0.0},
        }],
        dimension       : [{
            weight      : {type: Number, default:0.0},
            long        : {type: Number, default:0.0},
            height      : {type: Number, default:0.0},
        }]
    }],
    create              : {
        by              : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date            : {type: Date, default:  Date.now},
    },
    update              : [{
        by              : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date            : {type: Date, default:  Date.now},
    }]
});

var Config_CarTypes = mongoose.model('Config_CarTypes', Config_CarTypesSchema, 'Config_CarTypes');
module.exports = Config_CarTypes;
'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Config_JobTypesSchema  = new Schema({
    company             : {type: Schema.ObjectId, ref: 'Infomation_Companys'},
    title               : {type: String, default:null},
    description         : {type: String, default: null},
    key                 : {type: Number, default: null},
    enable              : {type: Boolean, default: true},
    create              : {
        by              : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date            : {type: Date, default:  Date.now}
    },
    update              : [{
        by              : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date            : {type: Date, default:  Date.now}
    }]
});

var Config_JobTypes = mongoose.model('Config_JobTypes', Config_JobTypesSchema, 'Config_JobTypes');
module.exports = Config_JobTypes;
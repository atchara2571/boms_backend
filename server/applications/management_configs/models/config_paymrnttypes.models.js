'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Config_PaymentTypesSchema  = new Schema({
    company             : {type: Schema.ObjectId, ref: 'Infomation_Companys'},
    title               : {type: String, default:null},
    val                 : {type: Number, default: 0},
    description         : {type: String, default: null},
    enable              : {type: Boolean, default: true},
    create              : {
        by              : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date            : {type: Date, default:  Date.now}
    },
    update              : [{
        by              : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date            : {type: Date, default:  Date.now}
    }]
});

var Config_PaymentTypes = mongoose.model('Config_PaymentTypes', Config_PaymentTypesSchema, 'Config_PaymentTypes');
module.exports = Config_PaymentTypes;
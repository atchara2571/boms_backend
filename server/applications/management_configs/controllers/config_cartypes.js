var utils = require('../../../../helpers/utils');
var infomations_cars = require('../models/config_cartypes.model');
var resMsgs = require('../../management_messages/controllers/management_messages');
var mongo = require('mongodb');


exports.onQuery_Cars = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onQuery_Cars";
    var query = {};
    query.enable = true;
    (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

    try {
        infomations_cars
            .find(query)
            .lean()
            .exec(function (err, doc) {
                for(i=0; i<doc.length; i++){
                    delete doc[i].update;
                    delete doc[i].create;
                    delete doc[i].__v
                }
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onCreate_Cars = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onCreate_Cars";
    try {

        var create = {};
        // create.date = startTime;
        (request.body.server_by != null) ? create.by = new mongo.ObjectID(request.body.server_by) : null;

        var data = {};
        (request.body.company != null) ? data.company = request.body.company : null;
        (request.body.title != null) ? data.title = request.body.title : null;
        (request.body.description != null) ? data.description = request.body.description : null;
        (request.body.enable != null) ? data.enable = request.body.enable : null;
        (request.body.support != null) ? data.support = request.body.support : null;

        data.create = create;

        var infomations_car = new infomations_cars(data);
        infomations_car.save(function (err, doc) {
            if (!err) {
                resMsgs.onMessage_Response(0,20000,function(res){
                    var resData = res;
                    resData.data = doc;
                    response.status(200).json(resData);
                    utils.writeLog(command, request, startTime, res, err)
                });
            } else {
                resMsgs.onMessage_Response(0,50002,function(res){
                    response.status(500).json(res);
                    utils.writeLog(command, request, startTime, res, err)
                });
            }
        });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onUpdate_Cars = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onUpdate_Cars";
    try {

        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        var data = {};
        (request.body.company != null) ? data.company = request.body.company : null;
        (request.body.title != null) ? data.title = request.body.title : null;
        (request.body.description != null) ? data.description = request.body.description : null;
        (request.body.enable != null) ? data.enable = request.body.enable : null;
        (request.body.support != null) ? data.support = request.body.support : null;

        var update = {};
        (request.body.server_by != null)? update.by = request.body.server_by : null;
        // update.date = startTime;

        var push = {};
        push.update =  update;
        data.$push = push;

        infomations_cars
            .findOneAndUpdate(query,data,{new:true})
            .lean()
            .exec(function (err, doc) {
                delete doc.update;
                delete doc.create;
                delete doc.__v;
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onResponse_Messages(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onCheckConfigPrice = function (_id, res) {

    var query = {};
    query._id = new mongo.ObjectID(_id);

    infomations_cars
        .findOne(query)
        .lean()
        .exec(function (err, doc) {
            if (err) {
                res(true,nil);
            } else {
                if (doc != null) {
                    delete doc._id;
                    delete doc.title;
                    delete doc.description;
                    delete doc.enable;
                    delete doc.update;
                    delete doc.create;

                    res(false,doc);
                } else {
                    res(true,nil);
                }
            }
        });
};


exports.onUpdateConfigsSupportDimension = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onUpdateConfigs";
    try {
        var query_array = {};
        query_array.partner = request.params.id;

        var support = {};
        support.$elemMatch = query_array;

        var query = {};
        // query.company = request.params.id;
        query.support = support;


        var data = {$push : {"support.$.dimension":request.body.dimension}};

        infomations_cars
            .findOneAndUpdate(query,data,{ new: true })
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0, 200, function (res) {
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40401, function (res) {
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })

    } catch (err){
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.onDeleteConfigsSupportDimension = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onUpdateConfigs";
    try {
        var query_array = {};
        query_array.partner = request.params.id;

        var support = {};
        support.$elemMatch = query_array;

        var query = {};
        // query.company = request.params.id;
        query.support = support;

        var data = {$pull : {"support.$.dimension":request.body.dimension}};

        infomations_cars
            .findOneAndUpdate(query,data,{ new: true })
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0, 200, function (res) {
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40401, function (res) {
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })

    } catch (err){
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.onUpdateConfigsSupportDistances = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onUpdateConfigs";
    try {
        var query_array = {};
        query_array.partner = request.params.id;

        var support = {};
        support.$elemMatch = query_array;

        var query = {};
        // query.company = request.params.id;
        query.support = support;


        var data = {$push : {"support.$.distances":request.body.distances}};

        infomations_cars
            .findOneAndUpdate(query,data,{ new: true })
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0, 200, function (res) {
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40401, function (res) {
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })

    } catch (err){
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.onDeleteConfigsSupportDistances = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onUpdateConfigs";
    try {
        var query_array = {};
        query_array.partner = request.params.id;

        var support = {};
        support.$elemMatch = query_array;

        var query = {};
        // query.company = request.params.id;
        query.support = support;

        var data = {$pull : {"support.$.distances":request.body.distances}};

        infomations_cars
            .findOneAndUpdate(query,data,{ new: true })
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0, 200, function (res) {
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40401, function (res) {
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })

    } catch (err){
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};


exports.onQuery_CarsType = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onQuery_CarsType";

    var query_array = {};
    query_array.partner = request.body.partner;

    var support = {};
    support.$elemMatch = query_array;

    var query = {};
    query.support = support;

    try {
        infomations_cars
            .findOne(query)
            .lean()
            .exec(function (err, doc) {

                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        for(i=0; i<doc.support.length; i++){
                            var data = doc.support[i];

                            if(data.partner == request.body.partner){
                                console.log(data.distances);
                                for(j=0; j<data.distances.length; j++){
                                    var service_distances = data.distances[j];

                                    console.log(request.body.total_distance);
                                    if(service_distances.number > (request.body.total_distance/1000)){
                                        request.body.total_price = service_distances.price;
                                        console.log("231");
                                        next();
                                        break
                                    }
                                }
                            }
                        }

                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};
var utils = require('../../../../helpers/utils');
var resMsgs = require('../../management_messages/controllers/management_messages');
var mongo = require('mongodb');

var payment_channels = require('../../management_payments/models/infomation_payment_channels.model');


exports.onPrices = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onUpdatePrices";
    try {
        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        payment_channels
            .findOne(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40401, function (res) {
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })
    }catch (err) {
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.onCreatePrices = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onCreatePrices";
    try {

        var create = {};
        (request.body.server.by != null)? create.by = request.body.server.by : null;
        create.date = startTime;

        var data = {};
        (request.body.company != null) ? data.company = request.body.company : null;
        (request.body.type != null) ? data.type = request.body.type : null;

        data.create = create;

        var price = new payment_channels(data);
        price.save(function (err, doc) {
            if(!err){
                resMsgs.onMessage_Response(0,20000,function(res){
                    var resData = res;
                    resData.data = doc;
                    response.status(200).json(resData);
                    utils.writeLog(command, request, startTime, res, err)
                });
            }else{
                resMsgs.onMessage_Response(0,50003,function(res){
                    response.status(500).json(res);
                    utils.writeLog(command, request, startTime, res, err)
                });
            }
        });

    }catch (err) {
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.onUpdatePrices = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onUpdatePrices";
    try {
        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        var push = {};
        var updates = {};
        (request.body.server_by != null)? updates.by = request.body.server_by : null;
        updates.date = startTime;

        push.update = updates;


        var data = {};

        (request.body.company != null) ? data.company = request.body.company : null;
        (request.body.type != null) ? data.type = request.body.type : null;

        data.$push = push;

        payment_channels
            .findOneAndUpdate(query,data,{ new: true })
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40401, function (res) {
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })
    }catch (err) {
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.onAddPrices_TypeByCompany = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onAddPrices_TypeByCompany";
    try {
        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        var push = {};
        var updates = {};
        (request.body.server.by != null)? updates.by = request.body.server.by : null;
        updates.date = startTime;

        push.update = updates;

        // (updates != null) ? push.update = updates : null;

        (request.body.type != null) ? push.type = request.body.type : null;


        var data = {};
        data.$push = push;

        payment_channels
            .findOneAndUpdate(query,data,{ new: true })
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40401, function (res) {
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })
    }catch (err) {
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.onUpdatePrices_TypeByCompany = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onAddPrices_TypeByCompany";
    try {

        var query_array = {};
        (request.params.id != null) ? query_array._id = new mongo.ObjectID(request.params.id) : null;

        var types = {};
        types.$elemMatch = query_array;

        var query = {};
        query.type = types;

        var set = {
            "type.$.val": (request.body.type.val != null) ? request.body.type.val : 0,
            "type.$.description": (request.body.type.description != null) ? request.body.type.description : null,
            "type.$.enable": (request.body.type.enable != null) ? request.body.type.enable : true,
            "type.$.name": (request.body.type.name != null) ? request.body.type.name : null
        };

        var push = {};
        var updates = {};
        (request.body.server.by != null)? updates.by = request.body.server.by : null;
        updates.date = startTime;
        push.update = updates;

        var data = {};
        data.$set = set;
        data.$push = push;

        payment_channels
            .findOneAndUpdate(query,data,{ new: true })
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40401, function (res) {
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })
    }catch (err) {
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.onDeletePrices_TypeByCompany = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onAddPrices_TypeByCompany";
    try {

        var query_array = {};
        (request.params.id != null) ? query_array._id = new mongo.ObjectID(request.params.id) : null;

        var types = {};
        types.$elemMatch = query_array;

        var query = {};
        query.type = types;

        var set = {
            "type.$.enable": false,
        };

        var push = {};
        var updates = {};
        (request.body.server_by != null)? updates.by = request.body.อ : null;
        updates.date = startTime;
        push.update = updates;

        var data = {};
        data.$set = set;
        data.$push = push;

        payment_channels
            .findOneAndUpdate(query,data,{ new: true })
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            // var resData = res;
                            // resData.data = doc;
                            response.status(200).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40401, function (res) {
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })
    }catch (err) {
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

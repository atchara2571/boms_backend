var utils = require('../../../../helpers/utils');
var multers = require('../../../../helpers/multer');
var configs = require('../../../../config/config');
var resMsgs = require('../../management_messages/controllers/management_messages');
var promotions = require('../models/management_promotions.model');

var mongo = require('mongodb');
var multer = require('multer');

var storage = multers.onConfigMulter(configs.images_part.promotion);
var upload = multer({ storage : storage }).single('image');

exports.onCreatePromotion = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onCreatePromotion";
    try {
        var create = {};
        (request.body.server_by != null)? create.by = request.body.server_by : null;
        create.date = startTime;

        var data = {};
        (request.body.title != null) ? data.title = request.body.title : null;
        (request.body.description != null) ? data.description = request.body.description : null;
        (request.body.company != null) ? data.company = request.body.company : null;
        (request.body.promote != null) ? data.promote = request.body.promote : null;
        (request.body.usage != null) ? data.usage = request.body.usage : null;
        (request.body.discount != null) ? data.discount = request.body.discount : null;
        (request.body.enable != null) ? data.enable = request.body.enable : null;

        if(request.body.items != null){
            var cards = [];
            for(i=0; i<request.body.items; i++){
                var card = {};
                card.code = utils.randomString(14);
                card.enable = false
                cards.push(card);
            }
            data.items = cards;
        }

        data.create = create;

        var promotion = new promotions(data);
        promotion.save(function (err, doc) {
            if(!err){
                resMsgs.onMessage_Response(0,20000,function(res){
                    var resData = res;
                    resData.data = doc;
                    response.status(200).json(resData);
                    utils.writeLog(command, request, startTime, res, err)
                });
            }else{
                console.log(err);
                resMsgs.onMessage_Response(0,50003,function(res){
                    response.status(500).json(res);
                    utils.writeLog(command, request, startTime, res, err)
                });
            }
        });

    }catch (err) {
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.onUpload_Images = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = 'onUpload_Images';

    upload(request, response, function (err) {
        request.body.image = (request.file != null) ? request.file.filename : "";
        return next();
    });
}

exports.onUpdate_Promotion = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onCreatePromotion";
    try {

        var query = {};
        (request.params.id != null) ? query._id = request.params.id : null;


        var data = {};
        (request.body.title != null) ? data.title = request.body.title : null;
        (request.body.description != null) ? data.description = request.body.description : null;
        (request.body.enable != null) ? data.enable = request.body.enable : null;
        (request.body.image != null) ? data.image = request.body.image : null;

        var update = {};
        (request.body.server_by != null)? update.by = request.body.server_by : null;
        update.date = startTime;

        var push = {};
        push.update =  update;
        data.$push = push;

        promotions
            .findOneAndUpdate(query,data,{ new: true })
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0, 200, function (res) {
                            response.status(200).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40401, function (res) {
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })

    } catch (err){
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.onCheckPromotion_Code = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onCreatePromotion";
    try {
        var query_array = {};
        query_array.code = request.params.id;
        query_array.enable = false;

        var items = {};
        items.$elemMatch = query_array;

        var query = {};
        query.items = items;


        promotions
            .findOne(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0, 200, function (res) {
                            response.status(200).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40401, function (res) {
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })

    } catch (err){
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.onUsePromotion_Code = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onCreatePromotion";
    try {
        var query_array = {};
        query_array.code = request.params.id;
        query_array.enable = false;

        var items = {};
        items.$elemMatch = query_array;

        var usage = { "by" : request.header.profile_information_id, "date" : startTime };

        var query = {};
        query.items = items;

        var data = {$set : {"items.$.enable" : true,"items.$.usage":usage}};
        console.log(query);
        console.log(data);
        promotions
            .findOneAndUpdate(query,data,{ new: true })
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0, 200, function (res) {
                            response.status(200).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40401, function (res) {
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })

    } catch (err){
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.onCheckPromotion_Discount = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onCreatePromotion";
    try {
        var query_array = {};
        query_array.code = request.params.id;
        query_array.enable = false;

        var items = {};
        items.$elemMatch = query_array;

        var query = {};
        query.items = items;

        promotions
            .findOne(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        delete doc.items;

                        var total_price = request.body.total_price;
                        var total_price = 1000;

                        if(total_price >= doc.discount.minimum_total){
                            if(doc.discount.price > 0){
                                total_price = (total_price-doc.discount.price);
                            }
                            if(doc.discount.rate > 0){
                                total_price = total_price-((total_price/100)*doc.discount.rate)
                            }
                        }
                        request.body.total_price = total_price;

                    } else {
                        resMsgs.onMessage_Response(0, 40401, function (res) {
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })

    } catch (err){
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

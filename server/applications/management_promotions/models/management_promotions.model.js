'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Management_PromotionsSchema  = new Schema({
    title           : {type: String, required: true},
    description     : {type: String, default: null},
    image           : [
        {type: String, default: null}
    ],
    company         : [{type: Schema.ObjectId, ref: 'Infomation_Companeys'}],
    promote         : {
        start : {type: Date, required: true},
        end   : {type: Date, required: true}
    },
    usage           : {
        start : {type: Date, required: true},
        end   : {type: Date, required: true}
    },
    discount     : {
        minimum_piece   : {type: Number, default: 0},
        minimum_total   : {type: Number, default: 0},
        price           : {type: Number, default: 0},
        rate            : {type: Number, default: 0}
    },
    items           : [
        {
            code            : {type: String, required: true, unique : true},
            enable          : {type: Boolean, default: true},
            usage           : {
                by          : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
                date        : {type: Date, default: null}
            }
        }
    ],
    enable          : {type: Boolean, default: true},
    create          : {
        by          : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date        : {type: Date, default:  Date.now}
    },
    update          : [{
        by          : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date        : {type: Date, default:  Date.now}
    }]

});

var Management_Promotions = mongoose.model('Management_Promotions', Management_PromotionsSchema, 'Management_Promotions');
module.exports = Management_Promotions;

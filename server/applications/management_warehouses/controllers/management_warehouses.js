var utils = require('../../../../helpers/utils');
var infomations_warehouses = require('../models/infomations_warehouses.model');
var resMsgs = require('../../management_messages/controllers/management_messages');
var mongo = require('mongodb');


exports.onQuery_Warehouses = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onQuery_Warehouses";
    var query = {};

    (request.params.partners != null) ? query.partners  = request.params.partners : null;

    try {
        infomations_warehouses
            .find(query)
            .populate("users.by","title contact invoice enable")
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onQuery_WarehouseById = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onQuery_WarehouseById";
    var query = {};
    (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;
    query.enable = true;

    try {
        infomations_warehouses
            .findOne(query)
            .populate("partners","title contact invoice enable")
            .lean()
            .exec(function (err, doc) {

                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

// exports.onQuery_WarehouseByIdCompany = function (request, response, next) {
//     var startTime = utils.getTimeInMsec();
//     var command = "onQuery_WarehouseByIdCompany";
//     var query = {};
//     (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;
//     query.enable = true;
//
//     try {
//         infomations_warehouses
//             .findOne(query)
//             .lean()
//             .exec(function (err, doc) {
//
//                 if (err) {
//                     resMsgs.onMessage_Response(0,50002,function(res){
//                         response.status(500).json(res);
//                         utils.writeLog(command, request, startTime, res, err)
//                     });
//                 } else {
//                     if (doc != null) {
//                         resMsgs.onMessage_Response(0,20000,function(res){
//                             var resData = res;
//                             resData.data = doc;
//                             response.status(200).json(resData);
//                             utils.writeLog(command, request, startTime, res, err)
//                         });
//                     } else {
//                         resMsgs.onMessage_Response(0,40401,function(res){
//                             response.status(404).json(res);
//                             utils.writeLog(command, request, startTime, res, err)
//                         });
//                     }
//                 }
//             });
//     } catch (err) {
//         resMsgs.onMessage_Response(0,50000,function(res){
//             response.status(500).json(res);
//             utils.writeLog(command, request, startTime, res, err)
//         });
//     }
// };

exports.onQuery_WarehouseByCompanyId = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onQuery_WarehouseByCompanyId";

    var query_array = {};
    query_array.company = new mongo.ObjectID(request.params.id);

    var items = {};
    items.$elemMatch = query_array;

    var query = {};
    query.support = items;
    query.enable = true;

    try {
        infomations_warehouses
            .find(query)
            .populate("partners","title contact invoice enable")
            .lean()
            .exec(function (err, doc) {


                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onCreate_Warehouse = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onCreate_Warehouse";
    try {

        var create = {};
        create.date = startTime;
        (request.body.server_by != null) ? create.by = new mongo.ObjectID(request.body.server_by) : null;

        var data = {};
        (request.body.company != null) ? data.company = request.body.company : null;
        (request.body.title != null) ? data.title = request.body.title : null;
        (request.body.description != null) ? data.description = request.body.description : null;
        (request.body.support != null) ? data.support = request.body.support : null;
        (request.body.enable != null) ? data.enable = request.body.enable : null;
        (request.body.partners != null) ? data.partners = request.body.partners : null;

        data.create = create;

        var infomations_warehouse = new infomations_warehouses(data);
        infomations_warehouse.save(function (err, doc) {
            if (!err) {
                resMsgs.onMessage_Response(0,20000,function(res){
                    var resData = res;
                    resData.data = doc;
                    response.status(200).json(resData);
                    utils.writeLog(command, request, startTime, res, err)
                });
            } else {
                resMsgs.onMessage_Response(0,50002,function(res){
                    response.status(500).json(res);
                    utils.writeLog(command, request, startTime, res, err)
                });
            }
        });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onUpdate_Warehouse = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onUpdate_Warehouse";
    try {

        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        var data = {};
        (request.body.company != null) ? data.company = request.body.company : null;
        (request.body.title != null) ? data.title = request.body.title : null;
        (request.body.description != null) ? data.description = request.body.description : null;
        (request.body.support != null) ? data.support = request.body.support : null;
        (request.body.enable != null) ? data.enable = request.body.enable : null;

        (request.body.partners != null)? data.partners = request.body.partners : null;

        var update = {};
        (request.body.server_by != null)? update.by = request.body.server_by : null;
        update.date = startTime;

        var push = {};
        push.update =  update;
        data.$push = push;

        infomations_warehouses
            .findOneAndUpdate(query,data,{new:true})
            .lean()
            .exec(function (err, doc) {

                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {

                        delete doc.update;
                        delete doc.create;
                        delete doc.__v;

                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onResponse_Messages(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};


exports.onWarehouse_AddCompanyById = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onWarehouse_AddCompanyById";
    try {
        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        var data = {};

        var support = {};
        (request.body.support != null)? support.$each = request.body.support : null;


        var update = {};
        (request.body.server_by != null)? update.by = request.body.server_by : null;
        update.date = startTime;

        var push = {};
        push.update =  update;
        (request.body.support != null)? push.support = support : null;
        data.$push = push;

        console.log(data);

        infomations_warehouses
            .findOneAndUpdate(query,data,{new:true})
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,50003,function(res){
                            response.status(500).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });

    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onWarehouse_DeleteCompanyById = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onCompanys_Update";
    try {
        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        var data = {};

        // var support = [];
        // (request.body.support != null)? support.$in = request.body.support : null;


        var update = {};
        (request.body.server_by != null)? update.by = request.body.server_by : null;
        update.date = startTime;

        var push = {};
        var pull = {};
        push.update =  update;
        (request.body.support != null)? pull.support = request.body.support : null;
        data.$pull = pull;
        data.$push = push;


        infomations_warehouses
            .findOneAndUpdate(query,data,{new:true})
            .lean()
            .exec(function (err, doc) {
                console.log(err);
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,50003,function(res){
                            response.status(500).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    //
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};


exports.onCheck_WarehouseSupport = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onCheck_WarehouseSupport";


    var query_array = {};
    query_array.company = request.body.company;

    var support = {};
    support.$elemMatch = query_array;

    var query = {};
    query.support = support;
    query.enable = true;

    try {
        infomations_warehouses
            .findOne(query)
            .lean()
            .exec(function (err, doc) {

                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        var data = doc.support;
                        for(i=0; i<data.length; i++){
                            if(request.body.company == data[i].company){
                                request.body.charged_warehouse = data[i].charged_warehouse;
                                next();
                            }
                        }
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};
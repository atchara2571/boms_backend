var utils = require('../../../../helpers/utils');
var infomations_store_products = require('../models/infomations_store_products.model');
var resMsgs = require('../../management_messages/controllers/management_messages');
var mongo = require('mongodb');

exports.onQuery_Store_Products = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onQuery_Store_Products";
    var query = {};
    // query.enable = true;
    (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;
    (request.params.company != null) ? query.company = new mongo.ObjectID(request.params.company) : null;
    (request.params.warehouse != null) ? query.warehouse = new mongo.ObjectID(request.params.warehouse) : null;
    (request.params.partner != null) ? query['partner.import'] = new mongo.ObjectID(request.params.partner) : null;

    try {
        infomations_store_products
            .find(query)
            .populate("company","title description image")
            .populate("warehouse","title description enable")
            .populate("partner.import","title contact")
            .populate("partner.export","title contact")
            .populate({path : 'serial_product',select:'title description type enable image serial_product', populate : {path : 'type',select:'title description enable'}})
            .sort('serial_product')
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onQuery_Store_Product = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onQuery_Store_Product";
    var query = {};
    // query.enable = true;
    (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;
    // (request.params.company != null) ? query.company = new mongo.ObjectID(request.params.company) : null;
    // (request.params.warehouse != null) ? query.warehouse = new mongo.ObjectID(request.params.warehouse) : null;

    try {
        infomations_store_products
            .findOne(query)
            .populate("company","title description image")
            .populate("warehouse","title description enable")
            .populate("partner.import","title contact")
            .populate("partner.export","title contact")
            .populate("serial_product","title description type enable image serial_product ")
            .sort('serial_product')
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};


exports.onQuery_Store_ProductBycompanyAndwarehouseSum = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onQuery_Store_Product";
    var query = {};
    // query.enable = true;
    (request.params.v != null) ? query.company = new mongo.ObjectID(request.params.company) : null;
    (request.params.warehouse != null) ? query.warehouse = new mongo.ObjectID(request.params.warehouse) : null;

    (request.params.partner != null) ? query['partner.import'] = new mongo.ObjectID(request.params.partner) : null;

    try {
        infomations_store_products
            .find(query)
            .populate("company","title description image")
            .populate("warehouse","title description enable")
            .populate("partner.import","title contact")
            .populate("partner.export","title contact")
            // .populate("serial_product","title description type enable image serial_product",)
            // .populate("serial_product.type")
            .populate({path : 'serial_product',select:'title description type enable image serial_product', populate : {path : 'type',select:'title description enable'}})
            .sort('serial_product')
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            // console.log(doc);

                            var new_doc = [];

                            for(i=0;i<doc.length;i++){

                                // check code same
                                var serial_product = doc[i].serial_product.serial_product;
                                var exports = doc[i].partner.export;
                                var other = doc[i].partner.export_others;

                                for(j=0;j<new_doc.length;j++){

                                    if(serial_product == new_doc[j].serial_product.serial_product){
                                        new_doc[j].total = new_doc[j].total+1;
                                        if(exports != null){
                                            new_doc[j].remain = new_doc[j].remain+1;
                                        }else{
                                            if(other != null){
                                                new_doc[j].remain = new_doc[j].remain+1;
                                            }
                                        }
                                        break;
                                    }else{
                                        doc[i].total = 1;
                                        doc[i].remain = 0;

                                        if(exports != null){
                                            doc[i].remain = 1;
                                        }else{
                                            if(other != null){
                                                doc[i].remain = 1;
                                            }
                                        }
                                        new_doc.push(doc[i]);
                                        break;
                                    }
                                }

                                if(new_doc.length == 0){

                                    doc[i].total = 1;
                                    doc[i].remain = 0;
                                    if(exports != null){
                                        doc[i].remain = 1;
                                    }else{
                                        if(other != null){
                                            doc[i].remain = 1;
                                        }
                                    }
                                    new_doc.push(doc[i]);
                                }
                            }


                            for(i=0;i<new_doc.length;i++){
                                delete new_doc[i]._id;
                                delete new_doc[i].update;
                                delete new_doc[i].create;
                                delete new_doc[i].charged_product;
                                delete new_doc[i].date;
                                delete new_doc[i].partner;
                                delete new_doc[i].enable;
                                delete new_doc[i].tag_code;
                                if(request.params.warehouse == null){
                                    delete new_doc[i].warehouse;
                                    delete new_doc[i].charged_warehouse;
                                }
                            }

                            resData.data = new_doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onQuery_Store_ProductByWarehouses = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onQuery_Store_ProductByWarehouses";
    var query = {};
    // query.enable = true;
    (request.params.id != null) ? query.warehouse = new mongo.ObjectID(request.params.id) : null;

    try {
        infomations_store_products
            .find(query)
            .populate("company","title description image")
            .populate("warehouse","title description enable")
            .populate("partner.import","title contact")
            .populate("partner.export","title contact")
            .populate({path : 'serial_product',select:'title description type enable image serial_product', populate : {path : 'type',select:'title description enable'}})
            .sort('serial_product')
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onQuery_Store_ProductByPartners = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onQuery_Store_ProductByPartners";
    var query = {};
    // query.enable = true;
    (request.params.id != null) ? query['partner.import'] = new mongo.ObjectID(request.params.id) : null;

    try {
        infomations_store_products
            .find(query)
            .populate("company","title description image")
            .populate("warehouse","title description enable")
            .populate("partner.import","title contact")
            .populate("partner.export","title contact")
            .populate({path : 'serial_product',select:'title description type enable image serial_product', populate : {path : 'type',select:'title description enable'}})
            .sort('serial_product')
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};


exports.onQuery_Store_ProductByPartnersExport = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onQuery_Store_ProductByPartners";
    var query = {};
    // query.enable = true;
    (request.params.id != null) ? query['partner.export'] = new mongo.ObjectID(request.params.id) : null;

    try {
        infomations_store_products
            .find(query)
            .populate("company","title description image")
            .populate("warehouse","title description enable")
            .populate("partner.import","title contact")
            .populate("partner.export","title contact")
            .populate("serial_product","title description type enable image serial_product ")
            .sort('serial_product')
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onฺBeforeCreate_Store_Products = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onCreate_Store_Products";
    try {


        var data = {};
        (request.body.company != null) ? data.company = request.body.company : null;
        (request.body.warehouse != null) ? data.warehouse = request.body.warehouse : null;
        (request.body.jobsheet != null) ? data.jobsheet = request.body.jobsheet : null;
        (request.body.partner != null) ? data.partner = request.body.partner : null;


        (request.body.partner != null) ? data.partner = request.body.partner : null;
        (request.body.serial_product != null) ? data.serial_product = request.body.serial_product : null;
        (request.body.tag_code != null) ? data.tag_code = request.body.tag_code : null;

        var date = {};
        date.import = startTime;
        data.date = date;


        (request.body.charged_warehouse != null) ? data.charged_warehouse = request.body.charged_warehouse : null;
        (request.body.charged_product != null) ? data.charged_product = request.body.charged_product : null;
        (request.body.enable != null) ? data.enable = request.body.enable : null;






        var infomations_store_product = new infomations_store_products(data);
        infomations_store_product.save(function (err, doc) {
            if (!err) {
                resMsgs.onMessage_Response(0,20000,function(res){
                    var resData = res;
                    resData.data = doc;
                    response.status(200).json(resData);
                    utils.writeLog(command, request, startTime, res, err)
                });
            } else {
                resMsgs.onMessage_Response(0,50002,function(res){
                    response.status(500).json(res);
                    utils.writeLog(command, request, startTime, res, err)
                });
            }
        });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};


exports.onCreate_Store_Products = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onCreate_Store_Products";
    try {

        var create = {};
        (request.body.server_by != null) ? create.by = new mongo.ObjectID(request.body.server_by) : null;
        create.date = startTime;

        var data = {};
        (request.body.company != null) ? data.company = request.body.company : null;
        (request.body.warehouse != null) ? data.warehouse = request.body.warehouse : null;
        (request.body.jobsheet != null) ? data.jobsheet = request.body.jobsheet : null;
        (request.body.partner != null) ? data.partner = request.body.partner : null;


        (request.body.serial_product != null) ? data.serial_product = request.body.serial_product : null;


        var date = {};
        date.import = startTime;
        data.date = date;


        (request.body.charged_warehouse != null) ? data.charged_warehouse = request.body.charged_warehouse : null;
        (request.body.charged_product != null) ? data.charged_product = request.body.charged_product : null;
        (request.body.enable != null) ? data.enable = request.body.enable : null;
        data.create = create;

        data.tag_code = utils.randomString(2)+startTime;
        // console.log(data);

        var infomations_store_product = new infomations_store_products(data);
        infomations_store_product.save(function (err, doc) {
            if (!err) {
                resMsgs.onMessage_Response(0,20000,function(res){
                    var resData = res;
                    resData.data = doc;
                    response.status(200).json(resData);
                    utils.writeLog(command, request, startTime, res, err)
                });
            } else {
                console.log(err);
                resMsgs.onMessage_Response(0,50002,function(res){
                    response.status(500).json(res);
                    utils.writeLog(command, request, startTime, res, err)
                });
            }
        });



    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onExport_Store_Products = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onExport_Store_Products";
    try {

        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;
        (request.body.company != null) ? query.company = new mongo.ObjectID(request.body.company) : null;
        (request.body.warehouse != null) ? query.warehouse = new mongo.ObjectID(request.body.warehouse) : null;
        (request.body.serial_product != null) ? query.serial_product = new mongo.ObjectID(request.body.serial_product) : null;
        (request.body.tag_code != null) ? query.tag_code = request.body.tag_code : null;


        var data = {};
        //
        var update = {};
        (request.body.server_by != null)? update.by = request.body.server_by : null;
        update.date = startTime;

        var push = {};
        push.update =  update;

        var set = {};
        set['date.export'] = startTime;
        set['partner.export'] = request.body.partner.export;


        data.$push = push;
        data.$set = set;

        infomations_store_products
            .findOneAndUpdate(query,data,{new:true})
            .lean()
            .exec(function (err, doc) {
                console.log(err);
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,40401,function(res){
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });

    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};
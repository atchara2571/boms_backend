'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Infomation_Store_ProductsSchema  = new Schema({
    company             : {type: Schema.ObjectId, ref: 'Infomation_Companys'},
    warehouse           : {type: Schema.ObjectId, ref: 'Infomation_Warehouses'},
    transportations     : {type: Schema.ObjectId, ref: 'Infomation_Transportations'},
    partner             : {
        import          : {type: Schema.ObjectId, ref: 'Infomation_Partners'},
        export          : {type: Schema.ObjectId, ref: 'Infomation_Partners'},
        export_others   : {type: String, default: null},
    },
    serial_product      : {type: Schema.ObjectId, ref: 'Infomation_Product_Serials'},
    tag_code            : {type: String, required: true, unique: true},
    date                : {
        import          : {type: Date, default:null},
        export          : {type: Date, default:null},
    },
    charged_warehouse   : {
        import_value    : {type: Number, default:0.0},
        daily_freight   : {type: Number, default:0.0},
        export_value    : {type: Number, default:0.0},
    },
    charged_product     : {
        import_value    : {type: Number, default:0.0},
        daily_freight   : {type: Number, default:0.0},
        export_value    : {type: Number, default:0.0},
        transportation  : {type: Number, default:0.0},
    },
    enable              : {type: Boolean, default: false},
    create              : {
        by              : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date            : {type: Date, default:  Date.now}
    },
    update              : [{
        by              : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date            : {type: Date, default:  Date.now}
    }]
});

var Infomation_Store_Products = mongoose.model('Infomation_Store_Products', Infomation_Store_ProductsSchema, 'Infomation_Store_Products');
module.exports = Infomation_Store_Products;
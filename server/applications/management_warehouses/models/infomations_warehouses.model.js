'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Infomation_WarehousesSchema  = new Schema({
    title                   : {type: String, default: null},
    description             : {type: String, default: null},
    partners                : [{type: Schema.ObjectId, ref: 'Infomation_Partners'}],
    support                 : [{
        company             : {type: Schema.ObjectId, ref: 'Infomation_Companeys'},
        charged_warehouse   : {
            import_value    : {type: Number, default:0.0},
            daily_freight   : {type: Number, default:0.0},
            export_value    : {type: Number, default:0.0}
        },
    }],
    enable                  : {type: Boolean, default: false},
    create                  : {
        by                  : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date                : {type: Date, default:  Date.now}
    },
    update                  : [{
        by                  : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date                : {type: Date, default:  Date.now}
    }]
});

var Infomation_Warehouses = mongoose.model('Infomation_Warehouses',Infomation_WarehousesSchema, 'Infomation_Warehouses');
module.exports = Infomation_Warehouses;
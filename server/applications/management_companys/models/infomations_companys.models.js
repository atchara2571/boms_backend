'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Infomation_CompanysSchema  = new Schema({
    title                   : {type: String, default: null},
    description             : {type: String, default: null},
    image                   : {type: String, default: null},
    contact                 : {
        address             : {type: String, default: null},
        lat                 : {type: String, default: null},
        long                : {type: String, default: null},

        telephone           : {type: String, required: true},
        email               : {type: String, required: true},
        line                : {type: String, default: null},
        facebook            : {type: String, default: null},
        website             : {type: String, default: null},
    },
    config                  : {type: Schema.ObjectId, ref: 'Infomations_Company_Configs'},
    enable                  : {type: Boolean, default: false},
    users                   : [{
        by                  : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        // type                : {type: Schema.ObjectId, ref: 'Config_UserTypes'},
        status              : {type: Boolean, default: false},
    }],
    create                  : {
        by                  : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date                : {type: Date, default:  Date.now}
    },
    update                  : [{
        by                  : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date                : {type: Date, default:  Date.now}
    }]
});

var Infomation_Companys = mongoose.model('Infomation_Companys', Infomation_CompanysSchema, 'Infomation_Companys');
module.exports = Infomation_Companys;
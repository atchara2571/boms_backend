'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Infomations_Company_ConfigsSchema   = new Schema({
    company                 : {type: Schema.ObjectId, ref: 'Infomation_Companys' },
    status                  : {type: Schema.key, ref: 'Company_Types' },
    expire_date             : {type: Date, default: null, required:true},
    promotion               : {
        enable              : {type: Boolean, default: true},
        limit_promotion     : {type: Number, default: 3},
        limit_card          : {type: Number, default: 100}
    },
    tracks                  : {
        products            : [{
            item            : {type: Schema.ObjectId, ref: 'Product_Usages' },
            device          : {type: Boolean, default: false },
            service         : {type: Boolean, default: false },
            enable          : {type: Boolean, default: false },
        }]
    },
    monitors                : {
        enable              : {type: Boolean, default: true},
        limit_account       : {type: Number, default: 1},
        credit              : {type: Number, default: 50},
        cuounts             : {type: Number, default: 0}
    },

    services                : {
        limit_Service       : {type: Number, default: 1},
        speed               : {type: Boolean, default: true},
        temperature         : {type: Boolean, default: true},
        petroleum           : {type: Boolean, default: true},
    },

    deliverys               : {
        enable              : {type: Boolean, default: true},
        credit              : {type: Number, default: 50},
        cuount              : {type: Number, default: 0}
    },
    fees                    : {type: Boolean, default: true},
    enable                  : {type: Boolean, default: true},
    create                  : {
        by                  : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date                : {type: Date, default:  Date.now}
    },
    update                  : [{
        by                  : {type: Schema.ObjectId, ref: 'Infomation_Accounts' },
        date                : {type: Date, default:  Date.now}
    }]

});

var Infomations_Company_Configs = mongoose.model('Infomations_Company_Configs', Infomations_Company_ConfigsSchema, 'Infomations_Company_Configs');
module.exports = Infomations_Company_Configs;

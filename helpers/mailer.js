/**
 * Created by thaigpstracker03 on 6/8/2018 AD.
 */
var querystring = require('querystring');
var http = require('https');

var logger = require('./logger');
var tag = '[MAILER] ';


exports.sendMail = function(mailTo, subject, message, filePath, fileName,callback){

    if(mailTo && subject && message){

        var data = {
            fromName : 'ThaiGPSTracker : BOMS Notification Center',
            to : encodeURIComponent(mailTo),
            subject : encodeURIComponent(subject),
            message : encodeURIComponent(message)
        };

        if(filePath && fileName){
            data.filepath = filePath;
            data.fileName = fileName;
        }

        var postData = querystring.stringify(data);

        var options = {
            hostname: 'http://mail.dd-gps.com',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': postData.length
            }
        };

        var req = http.request(options,function(res) {

            var body = '';
            res.setEncoding('utf8');
            res.on('data', function(chunk) {
                body += chunk;
            });

            res.on('end', function() {
                logger.log(tag+' Sent Mail To '+mailTo+' result => '+body);
                if(typeof callback != 'undefined'){
                    callback(body,null);
                }
            });

            req.on('error',function(e){
                logger.error(tag+' Sent Mail To '+mailTo+' error => '+JSON.stringify(e));
                if(typeof callback != 'undefined'){
                    callback(null,e);
                }
            });
        });


        logger.log(tag+' Sending Mail To '+mailTo);
        req.write(postData);
        req.end();

    }

};
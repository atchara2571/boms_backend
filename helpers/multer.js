var multer = require('multer');

var onConfigMulter = function (part, res) {
    var storage = multer.diskStorage({
        destination : function (req, file, cb) {
            cb(null, part)
        },
        filename : function (req, file, cb) {
            var ext = file.originalname.substring(file.originalname.lastIndexOf('.'), file.originalname.length);
            cb(null, Date.now() + ext)
        }
    });
    return storage;
};

exports.onConfigMulter = onConfigMulter;

exports.onUpload_Single_Images = function (request, response, next) {
    var genBody = multer().fields([]);
    genBody(request, response, function (err) {
        console.log(request.body);
        return;
    });
    var storage = onConfigMulter(request.headers.part);
    var upload = multer({ storage : storage }).single('image');
    upload(request, response, function (err) {
        request.body.image = (request.file != null) ? request.file.filename : "";
        return next();
    });
};
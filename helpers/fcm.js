/**
 * Created by thaigpstracker03 on 6/8/2018 AD.
 */

var querystring = require('querystring');
var https = require('https');

var cfg = require('../config/config');
var logger = require('./logger');
var tag = '[FCM] ';
var constant = {
    FCM_API_DOMAIN : "fcm.googleapis.com",
    FCM_API_SEND_PATH : "/fcm/send"
};
var FCMDataValidator = function(fields){

    var result = {
        valid : true,
        error : null
    };


    if(fields){
        if(!fields.registration_ids){
            result = {
                valid : false,
                error : ' Require Field : "registration_ids" '
            };
            return result;
        }
        if(!fields.notification){
            result = {
                valid : false,
                error : ' Require Field : "notification" '
            };
            return result;
        }
        if(fields.notification && !fields.notification.title){
            result = {
                valid : false,
                error : ' Require Field : "notification.title" '
            };
            return result;
        }
        if(fields.notification && !fields.notification.body){
            result = {
                valid : false,
                error : ' Require Field : "notification.body" '
            };
            return result;
        }
    }
    else{
        result = {
            valid : false,
            error : 'Parameter Required.'
        };
    }

    return result;

};

exports.sendNotification = function(fields){

    var validateResult = FCMDataValidator(fields);
    if(!validateResult.valid){
        logger.log(tag + 'Validator : Invalid (' + validateResult.error + ').');
        return;
    }

    var token = fields.registration_ids;

    var postData = querystring.stringify(data);

    var options = {
        hostname: constant.FCM_API_DOMAIN,
        port: 443,
        path: constant.FCM_API_SEND_PATH,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': postData.length,
            'Authorization': 'key='+cfg.fireBase.fcm.token
        }
    };

    var req = https.request(options,function(res) {

        var body = '';
        res.setEncoding('utf8');
        res.on('data', function(chunk) {
            body += chunk;
        });

        res.on('end', function() {
            logger.log(tag+' Sent Notification To '+token+' result => '+body);
            if(typeof callback != 'undefined'){
                callback(body,null);
            }
        });

        req.on('error',function(e){
            logger.error(tag+' Sent Notification To '+token+' error => '+JSON.stringify(e));
            if(typeof callback != 'undefined'){
                callback(null,e);
            }
        });
    });


    logger.log(tag+' Sending Notification To '+token);
    req.write(postData);
    req.end();

};

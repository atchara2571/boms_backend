/**
 * Created by thaigpstracker03 on 6/7/2018 AD.
 */
var Base64 = require('js-base64').Base64;
var querystring = require('querystring');
var https = require('https');

var cfg = require('../config/config');
var logger = require('./logger');

var tag = '[LINE NOTIFY] ';
var constant = {
    LINE_NOTIFY_DOMAIN : "notify-bot.line.me",
    LINE_NOTIFY_OAUTH_AUTHORIZE : "/oauth/authorize",
    LINE_NOTIFY_OAUTH_TOKEN : "/oauth/token",
    LINE_NOTIFY_SEND_MESSAGE : "/api/notify",
    TOKEN_KEY : ".TLN.",
    KEY_GEN_LV1 : "g\$fen*tf",
    KEY_GEN_LV2 : "G87Suzjd",
};

var notify = function(token,data,callback){

     if(!token){

         if(typeof callback != 'undefined'){
             callback(null,'Line Notify Token required.');
         }
     }

    var postData = querystring.stringify(data);

    var options = {
        hostname: constant.LINE_NOTIFY_DOMAIN,
        port: 443,
        path: constant.LINE_NOTIFY_SEND_MESSAGE,
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': postData.length,
            'Authorization': 'Bearer '+token
        }
    };

    var req = https.request(options,function(res) {

          var body = '';
          res.setEncoding('utf8');
          res.on('data', function(chunk) {
              body += chunk;
          });

          res.on('end', function() {
              logger.log(tag+' Sent Notify To '+token+' result => '+body);
              if(typeof callback != 'undefined'){
                  callback(body,null);
              }
          });

          req.on('error',function(e){
              logger.error(tag+' Sent Notify To '+token+' error => '+JSON.stringify(e));
              if(typeof callback != 'undefined'){
                  callback(null,e);
              }
          });
     });


    logger.log(tag+' Sending Notify To '+token);
    req.write(postData);
    req.end();
};

exports.notifyMessage = function(token,message,callback){
     var data = {
          message : message
     };
     notify(token,data,callback);
};

exports.notifySticker = function(token,packageId,stickerId,callback){
     var data = {
         stickerPackageId : packageId,
         stickerId : stickerId
     };
     notify(token,data,callback);
};

exports.notifyImage = function(token,imageUrl,callback){
     var data = {
         imageFile : imageUrl
     };
     notify(token,data,callback);
};


exports.encodeToken = function(username,server){

     try{
         var eData = (username || '') + constant.TOKEN_KEY + (server || '');
         eData = eData + constant.KEY_GEN_LV1;
         var s1 = Base64.encode(eData);
         if(s1 && s1.length > 0){
             if(s1.indexOf('==') == (s1.length -2)){
                 s1 = s1.substr(0,s1.length -2);
             }
             else if(s1.indexOf('=') == (s1.length -1)){
                 s1 = s1.substr(0,s1.length -1);
             }
         }
         s1 = Base64.encode( s1 + constant.KEY_GEN_LV2);
         return s1;
     }
     catch (e){
         logger.error(e);
     }
     return null;
};

exports.decodeToken = function(token){

     var result = {
         valid: false,
         username: null,
         server: null
     };

     if(token && token.length > 0){

          try{

              var de = Base64.decode(token);
              if(de && de.length > 0){
                  de = de.substr(0,de.indexOf(constant.KEY_GEN_LV2));
                  de = Base64.decode(de);
                  if(de && de.length > 0){
                      de = de.substr(0,de.indexOf(constant.KEY_GEN_LV1));
                      var data = de.split(constant.TOKEN_KEY);
                      if(data || data.length >= 2){
                          result = {
                              valid: true,
                              username: data[0],
                              server: data[1]
                          };
                      }
                  }
              }

          }
          catch (e){
              logger.error(e);
          }
     }


     return result;
};

exports.generateLineNotifyAuthorizeUrl = function(state_token){

    var data = querystring.stringify({
        'response_type' :'code',
        'client_id' : cfg.lineOfficial.clientID,
        'redirect_uri' : cfg.lineOfficial.authorizeCallback,
        'scope' : 'notify',
        'state' : state_token
    });

    return "https://"+ constant.LINE_NOTIFY_DOMAIN + constant.LINE_NOTIFY_DOMAIN+constant.LINE_NOTIFY_OAUTH_AUTHORIZE + "?" + data;
};

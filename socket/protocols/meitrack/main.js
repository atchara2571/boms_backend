/**
 * Created by thaigpstracker03 on 6/14/2018 AD.
 */

var redis = require('redis');


var config = require('./config');
var tcpServer = require('../_helpers/tcpServer');
var db = require('../_helpers/db');

server = new tcpServer();


function onConnectMongo(isConnected){

    if(isConnected){
        var protocol = require('./protocol');
        var debug = require('../_helpers/debug');

        server.onReceiveMessage = function(client,data){

            debug(client,client.id,data);

        };
    }
}

db.connectMongo(config.connection.mongo,onConnectMongo);


module.exports = server;
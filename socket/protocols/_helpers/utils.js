/**
 * Created by thaigpstracker03 on 6/14/2018 AD.
 */

var debug = require('./debug');

exports.getHex = function(data){
    if (!data) return '';
    var hex = '';
    for (var i = 0; i< data.length;i++){
        c = data[i].toString(16);
        hex += (c.length <2)? '0'+c : c;
    }
    return hex;
};

exports.getDateFromLog = function(data){
    var gps_date = new Date();
    var year = parseInt(data[0]) + 2000;
    gps_date.setYear(year);

    var month = parseInt(data[1]) - 1;
    gps_date.setMonth(month);

    var day = parseInt(data[2]);
    gps_date.setDate(day);

    var hour = parseInt(data[3]) -1;
    gps_date.setHours(hour);

    var minute = parseInt(data[4]);
    gps_date.setMinutes(minute);

    var second = parseInt(data[5]);
    gps_date.setSeconds(second);

    return gps_date;
};

exports.convertDegreeToUMTS = function(data){
    var lipda = data / 30000;
    var a = parseFloat(Math.floor(lipda/60));
    var b = parseFloat((lipda%60)/60);
    // var c = parseFloat(Math.floor(b)/60);
    // var d = parseFloat((b%1)/36)

    return (a+b).toFixed(6);
};

exports.parseSignedHexToDec = function(hex){
    if (!hex) return NaN;

    try{
        hex = "0x"+hex;
        var dec = parseInt(hex,16);
        if((dec & 0x8000) > 0){
            dec = dec - 0x10000;
            dec = dec / 10000;
        }
        else{
            dec =dec /100;
        }
        return dec;
    }
    catch (e){
        return NaN;
    }

};

exports.readRFID = function(rfid){
    var pattern = [
        /%\s+\^(.*)\$(.*)\$(.*)\^(.*)\^\?\;(\d+)\=(\d+)\=\?\+\s+(.*)\?/
        ,/%\s+\^(.*)\$(.*)\$(.*)\^(.*)\^\?\;(\d+)\=(\d+)\=\?\+(.*)\?/
        ,/%\s+\^(.*)\$(.*)(.*)\^(.*)\^\?\;(\d+)\=(\d+)\=\?\+(.*)\?/
        ,/%\s+\^(.*)\$(.*)\$(.*)\^(.*)\^\?\;(\d+)\=(\d+)\=\?\+(.*)\?/
        ,/%\s+\^(.*)\$(.*)\$(.*)\^(.*)\^\?\r?\n\;(\d+)\=(\d+)\=\?\r?\n\+\s+(.*)\?/
        ,/%\s+\^(.*)\$(.*)\$(.*)\^(.*)\^\?\r?\n\;(\d+)\=(\d+)\=\?\r?\n\+(.*)\?/
        ,/%\s+(\w+)\s+(\w+)\s+(.*)\s+(\?)\r?\n\;(\d+)\=(\d+)\=\?\r?\n\+(.*)\?/
        ,/%\s+(\w+)\s+(\w+)\s+(.*)\s+(\?)\;(\d+)\=(\d+)\=\?\+(.*)\?/
        ,/%\s+\^(\w+)\s+(\w+)\s+(.*)\^(.*)\?\;(\d+)\=(\d+)\=\?\+\s+(.*)\?/
        ,/%\s+\^(.*)\$(.*)(.*)\^(.*)(.*)\^(.*)\?\+(.*)\?/
        ,/%\s+(.*)\s(.*)\s(.*)\s\>(.*)\;(\d+)\=(\d+)\=\?\+(.*)/
        ,/%\s+(.*)\s(.*)\s(.*)\s\>(.*)\;(\d+)\=(\d+)\=\?\+(.*)\?/
        ,/%\s+\^(.*)\s+(.*)\s+(.*)\^(.*)\^\?\n\;(\d+)\=(\d+)\=\?\n\+(.*)/
        ,/%\s+\^(.*)\s+(.*)\s+(.*)\^(.*)\^\?\n\;(\d+)\=(\d+)\=\?\n\+(.*)\?/
    ];
    // console.log("READ RFID DATA ",rfid);
    for(var i in pattern){
        var regx	=new RegExp(pattern[i]);
        var rfidData = regx.exec(rfid);

        // console.log("READ RFID DATA WITH PATTERN "+i+" result = ",(rfidData)?rfidData.length : null);
        if(rfidData && rfidData.length >=7){
            return rfidData;
        }
    }
    return null;
};

exports.checkRFIDIsLicense = function(rfid){

    if(rfid == undefined || rfid == null || rfid == ''){
        return false;
    }
    var rfidUnspace = rfid.replace(new RegExp(' ', 'g'), '');
    var isLicense = rfidUnspace.match('^[0-9]{15,}$');
    if(isLicense == null){
        return false;
    }
    else if(rfid.length < 15){
        return false;
    }else{
        var split = rfid.split(/\s+/);
        if(split[3] != undefined && split[3].length < 3){
            return false;
        }
        return true;
    }

};


exports.printLogData = function(client,log){


    if(log){
        var msg = {
            log_username : log.log_username || '-',
            log_dateupdated : log.log_dateupdated || '-',
            log_latlng : log.log_latlng || '-',
            log_SAT : log.log_SAT || '-',
            log_GSM : log.log_GSM || '-',
            log_speed : log.log_speed || '-',
            log_direction : log.log_direction || '-',
            log_State : log.log_State || '-',
            log_value : log.log_value || '-',
            log_milage : log.log_milage || '-',
            log_rfid : log.log_rfid || '-',
            log_temp : log.log_temp || '-'
        }
        if(log.log_temp2){
            msg.log_temp2 = log.log_temp2;
        }
        if(log.log_temp3){
            msg.log_temp3 = log.log_temp3;
        }
        if(log.log_temp4){
            msg.log_temp4 = log.log_temp4;
        }
        if(log.NAN){
            msg.NAN = log.NAN;
        }
        console.log("=============================================================================================================");
        debug(client,"[LOG] "+JSON.stringify(msg));
        console.log("=============================================================================================================");

    }

};
/**
 * Created by thaigpstracker03 on 6/14/2018 AD.
 */

var mongoose = require('mongoose');
var mongo = require('mongodb');
var mainConfig = require('../../../config/config');
var dataUtils = require('../../../helpers/dataUtils');
var debug = require('./debug');

var Logs_GpsTrackings = require('../../../server/applications/gps_service/models/Gps_Tracking_Logs.model.js');
var Geocode = require('../../../server/applications/third_party/models/geo_address/Tp_Geocode');
var Pois = require('../../../server/applications/third_party/models/geo_address/Tp_Pois');

const dbMongo = {
    connected : false
};
const geoMongo = {
    connected : false,
    db : null
};

function connectGeoMongo(){
    geoMongo.db = mongoose.createConnection(mainConfig.mongoGeo.url,{
        server: {
            poolSize: 10
        },
        user : mainConfig.mongoGeo.user,
        pass : mainConfig.mongoGeo.pass
    });
    geoMongo.db.on('error', function (err) {
        geoMongo.connected = false;
        console.error('----- Connect To Geo Mongo Error Status[' + JSON.stringify(err) + '] -----');
    });

    geoMongo.db.once('open', function () {
        geoMongo.connected = true;
        console.log('----- Connect To Geo Mongo Status[ Connected ] -----');
    });
}

exports.connectMongo = function(mongoUri,callback){
    if(mongoUri){
        mongoose.Promise = global.Promise;
        mongodb = mongoose.connect(mongoUri, {useMongoClient: true});
        var db = mongoose.connection;
        db.on('error', function (err) {
            dbMongo.connected = false;
            console.error('----- Connect To MongoDB Error Status[' + JSON.stringify(err) + '] -----');
            if(typeof callback !== 'undefined'){
                callback(false);
            }
        });

        db.once('open', function () {
            global.mongodb = db;
            dbMongo.connected = true;
            console.log('----- Connect To Mongodb Status[ Connected ] -----');
            connectGeoMongo();
            if(typeof callback !== 'undefined'){
                callback(true);
            }
        });

        dbMongo.db = db;
    }
};

exports.insertLog = function(client,log,callback){

    if(client && log && client.imei){

        try{
            var Log_Model = Logs_GpsTrackings.getCollectionModel(client.imei);
            if(Log_Model){
                var logModel = new Log_Model(log);
                logModel.save(function (err, doc) {
                    if (!err) {
                        debug(client,'Insert log : ERROR =>',err);
                        callback(null,err);
                    } else {
                        debug(client,'Insert log : OK');
                        exports.findGeoAddressAndUpdateLog(doc);
                        callback(doc,null);

                    }
                });
            }
        }
        catch (e){
            debug(client,'Insert log : ERROR =>',e);
        }
    }

};

exports.findGeoAddressAndUpdateLog = function(client,docLog){

    if(geoMongo.connected && docLog && docLog._id && docLog.latlng){

        var geocodeModel = Geocode.getModel(geoMongo.db);
        var poiModel = Pois.getModel(geoMongo.db);

        var query = {
            loc : {
                '$near' : docLog.latlng.split(','),
                '$maxDistance' : 0.08
            }
        };
        geocodeModel.findOne(query)
            .lean()
            .exec(function (err, doc) {
                if(!err && doc){

                    var data = {
                        address : dataUtils.parseGeocodeToShortAddr(doc)
                    };

                    updateLog(client,docLog,data);

                }
            });
        poiModel.findOne(query)
            .lean()
            .exec(function (err, doc) {
                if(!err && doc){

                    var data = {
                        poi : {
                            name : doc.name,
                            latlng : doc.latlng,
                            distance : dataUtils.calculateDistanceBetweenPoint(docLog.latlng,doc.latlng)
                        }
                    };

                    updateLog(client,docLog,data,function(doc,err){
                        if(!err){
                            debug(client,'log (id='+docLog._id+') : set Poi success.');
                        }
                    });

                }
            });


    }

};


function updateLog(client,docLog,data,callback){
    if(docLog && docLog._id && data && Object.keys(data).length > 0) {
        var query = {_id: docLog._id};


        var Log_Model = Logs_GpsTrackings.getCollectionModel(client.imei);
        if(Log_Model){
            Log_Model.findOneAndUpdate(query, data, {new: true})
                .lean()
                .exec(function (err, doc) {

                    if(err){
                        debug(client,'Update log (id='+docLog._id+') : ERROR =>',err);
                    }
                    else if(doc){
                        debug(client,'Update log (id='+doc._id+') : OK');
                    }

                    if(typeof callback == 'function'){
                        callback(doc,err);
                    }
                });
        }


    }
}
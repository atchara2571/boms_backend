/**
 * Created by thaigpstracker03 on 6/15/2018 AD.
 */
'use strict';

// Load the TCP Library
var net = require('net');
var md5 = require('md5');
var debug = require('./debug');

var getSocketId = function(socket){
    if(socket){
        return  socket.remoteAddress+":"+socket.remotePort;
    }
    return null;
};

var socketClient = function(socket) {

    var c = {};
    c.socket = socket.socket;
    c.address = socket.remoteAddress;
    c.port = socket.remotePort;
    c.id = getSocketId(socket);

    return c;
};

var server = function(){

    var s = {};
    s.clientList = {};
    s.clientIdList = {};
    s.onReceiveMessage = null;
    s.sendMessage = function(client,message){
        if(client && client.socket && message){

            debug(client,'sending Message: ',message);
            client.socket.write(message);
        }
    };
    s.onConnection = function(socket){

        var socketId = getSocketId(socket);
        if(!s.clientList[socketId]){
            console.log('New Client from ' + socketId+' [Connection Total : '+Object.keys(s.clientList).length+']');
            s.clientList[socketId] = new socketClient(socket);
        }

        socket.on('end', function() {
            var socketId = getSocketId(socket);
            var msg = 'Disconnected';
            if(s.clientList[socketId]){
                if(s.clientList[socketId].imei){
                    msg = '['+s.clientList[socketId].imei+']' + msg;
                }
                delete s.clientList[socketId];
            }
            console.log('[' + socketId+'] '+msg+'. [Connection Total : '+Object.keys(s.clientList).length+']');
        });
        socket.on('data',function(data){
            if(typeof s.onReceiveMessage == 'function'){
                var client = s.clientList[getSocketId(socket)];
                s.onReceiveMessage(client,data);
            }
        });

        socket.on('error',function(err){
            var msg = 'Error :';
            var socketId = getSocketId(socket);
            if(s.clientList[socketId] && s.clientList[socketId].imei){
                msg = '['+s.clientList[socketId].imei+']' + msg;
            }
            console.log('[' + socketId+']  '+msg,err);
        });

        socket.on('timeout',function(){
            var msg = 'Timed out';
            var socketId = getSocketId(socket);
            if(s.clientList[socketId] && s.clientList[socketId].imei){
                msg = '['+s.clientList[socketId].imei+']' + msg;
            }
            console.log('[' + socketId+'] '+msg+'.');
        });

        socket.on('drain',function(){
            var msg = 'Drain (write buffer becomes empty.)';
            var socketId = getSocketId(socket);
            if(s.clientList[socketId] && s.clientList[socketId].imei){
                msg = '['+s.clientList[socketId].imei+']' + msg;
            }
            console.log('['+socketId+'] '+msg+' . ');
        });

        socket.on('close',function(hadTransmissionError){

            var msg = 'Closed';
            if(hadTransmissionError){
                msg += ' with had transmission error';
            }
            var socketId = getSocketId(socket);
            if(s.clientList[socketId]){
                if(s.clientList[socketId].imei){
                    msg = '['+s.clientList[socketId].imei+']' + msg;
                }
                delete s.clientList[socketId];
            }

            console.log('[' + socketId+'] '+msg+'. [Connection Total : '+Object.keys(s.clientList).length+']');
        });

    };
    s.connection = net.createServer(s.onConnection);
    s.connection.on('error',function(error){
        console.log('TCP Server '+(s.port? 'On '+s.port : '')+" Error: ",error);

    });
    s.connection.on('close',function(){
        console.log('TCP Server '+(s.port? 'On '+s.port : '')+" Closed.");
    });

    s.start = function(port,callback){
        if(port){
            s.port = port;
            s.connection.listen(port,function(){
                if(typeof callback != 'undefined'){
                    callback(s.port);
                }
            });
        }
    };

    return s;
};

module.exports = server;